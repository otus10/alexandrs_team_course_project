﻿using DocumentFlow.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DocumentFlow.DataAccess.Interfaces.DataAccess
{
    public interface IDbContext
    {
        DatabaseFacade Database { get; }
        DbSet<Attachment> Attachments { get; set; }
        DbSet<Document> Documents { get; set; }
        DbSet<DocumentAttachment> DocumentAttachments { get; set; }
        DbSet<DocumentState> DocumentStates { get; set; }
        DbSet<DocumentStateTransaction> DocumentStateTransactions { get; set; }
        DbSet<DocumentTemplate> DocumentTemplates { get; set; }
        DbSet<DocumentType> DocumentTypes { get; set; }
        DbSet<Employee> Employees { get; set; }
        DbSet<EmployeeRouteStage> EmployeeRouteStages { get; set; }
        DbSet<Route> Routes { get; set; }
        DbSet<RouteBody> RouteBodies { get; set; }
        DbSet<RouteStage> RouteStages { get; set; }


        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

        int SaveChanges();
    }
}
