﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.Utils.Modules
{
   public abstract class Module
   {
      public IConfiguration Configuration { get; set; }

      public abstract void Load(IServiceCollection services);
   }
}
