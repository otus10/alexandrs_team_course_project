﻿using DocumentFlow.Utils.Modules;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.Utils.Extensions
{
   public static class ServiceCollectionExtensions
   {
      public static void RegisterModule<T>(this IServiceCollection serviceCollection, IConfiguration configuration) where T : Module
      {
         var module = Activator.CreateInstance<T>();

         module.Configuration = configuration;

         module.Load(serviceCollection);
      }
   }
}
