```sql
with cte_jobs as
(
    select 'Developer' as JobTitle, 0 as Id
    union all 
    select 'TeamLead', 1 as Id
    union all 
    select 'ProjectManager', 2 as Id
    union all 
    select 'ChiefDevelopmentDepartment', 3 as Id
    union all 
    select 'HRManager', 4 as Id
    union all 
    select 'ChiefHR', 5 as Id
    union all 
    select 'Accountant', 6 as Id
    union all 
    select 'SeniorAccountant', 7 as Id
    union all 
    select 'ChiefAccountant', 8 as Id
    union all 
    select 'FinancialDirector', 9 as Id
    union all 
    select 'GeneralDirector', 10 as Id
)
,cte_result as
(
    select 'Initiate' as result, 0 AS id 
    union all    
    select 'Approve' as result, 1 AS id 
    union all    
    select 'Reject' as result, 2 AS id 
    union all
    select 'Cancel' as result, 3 AS id 
    union all
    select 'OnHold' as result, 4 AS id
    union all
    select 'StartRoute' as result, 5 AS id
)
,cte_state as
(
    select 'Created' as state, 0 AS id 
    union all
    select 'InRouting' as state, 1 AS id 
    union all
    select 'Finished' as state, 2 AS id
)
select 
     d."Name"         as "Document"
    ,dt1."NameEn"     as "DocumentType"
    ,e."FullName"     as "InitiatorName"
    ,ji.JobTitle      as "InitiatorJobTitle"
    ,e1."FullName"    as "ApproverName"
    ,ji1.JobTitle     as "ApproverJobTitle"
    ,r.result         as "PreviousAction"
    ,dst."Comment"
    ,rs."Description" as "NextStage"
    ,dst."DateCreated"
    ,dst."DateUpdated"
    ,st.state
    ,dst."RouteStageOrder"
    ,dst."Id"
    ,dst."InitiatorDocumentStateId"
    ,dst."PreviousDocumentStateId"
FROM PUBLIC."DocumentStateTransactions" dst
left join "Documents" d                 on d."Id" = dst."DocumentId"
left join "DocumentTemplates" dt        on dt."Id" = d."DocumentTemplateId"
left join "DocumentTypes" dt1           on dt1."Id" = dt."DocumentTypeId"
left join "Employees" e                 on d."InitiatorId" = e."Id"
left join "Employees" e1                on dst."EmployeeId" = e1."Id"
left join cte_jobs ji                   on ji.id = e."JobTitle"
left join cte_jobs ji1                  on ji1.id = e1."JobTitle"
left join cte_result r                  on r.id = dst."Result"
left join "RouteStages" rs              on dst."RouteStageNextId" = rs."Id"
left join cte_state st                  on st.id = dst."State"
where d."Id" = 22
```