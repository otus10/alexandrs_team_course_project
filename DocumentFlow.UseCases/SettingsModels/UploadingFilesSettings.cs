﻿namespace DocumentFlow.UseCases.SettingsModels
{
    public class UploadingFilesSettings
    {
        public string Directory { get; set; }
    }
}
