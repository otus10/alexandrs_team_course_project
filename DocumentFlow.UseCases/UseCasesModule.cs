﻿using DocumentFlow.Utils.Modules;
using DocumentFlow.UserCases.Handlers.Documents.Queries.GetDocumentType;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace DocumentFlow.UserCases
{
   public class UseCasesModule : Module
   {

      public override void Load(IServiceCollection services)
      {
         services.AddMediatR(typeof(GetDocumentType.Handler));
      }
   }
}
