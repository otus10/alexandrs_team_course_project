﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DocumentFlow.DataAccess.Interfaces.DataAccess;
using DocumentFlow.Entities.Models;
using DocumentFlow.UseCases.Managers.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DocumentFlow.UseCases.Managers
{
    public class DocumentManager : IDocumentManager
    {
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;

        public DocumentManager(IDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<DocumentState> CreateDocumentState(Document document, CancellationToken cancellationToken)
        {
            DocumentStateTransaction documentStateTransaction = await InitDocumentStateTransaction(document.Id, cancellationToken);

            DocumentState documentState = _mapper.Map<DocumentState>(documentStateTransaction);
            documentState.CurrentDocumentStateId = documentStateTransaction.Id;

            _dbContext.DocumentStates.Add(documentState);

            return documentState;
        }

        private async Task<DocumentStateTransaction> InitDocumentStateTransaction(int documentId, CancellationToken cancellationToken)
        {
            Document document = _dbContext.Documents.Where(w => w.Id == documentId)
                .Include(i => i.DocumentTemplate)
                .ThenInclude(i => i.Route)
                .ThenInclude(i => i.RouteBodies)
                .FirstOrDefault();

            DocumentStateTransaction documentStateTransaction = new()
            {
                DocumentId = document.Id,
                EmployeeId = document.InitiatorId,
                Result = Result.Initiate,
                RouteStageNextId = GetNextRouteStage(document.DocumentTemplate.RouteId, 0).Id,
                InitiatorDocumentStateId = null,
                PreviousDocumentStateId = null,
                Comment = string.Empty,
                State = State.Created
            };

            await _dbContext.DocumentStateTransactions.AddAsync(documentStateTransaction);
            await _dbContext.SaveChangesAsync(cancellationToken);

            documentStateTransaction.InitiatorDocumentStateId = documentStateTransaction.Id;

            return documentStateTransaction;
        }

        public async Task<DocumentStateTransaction> UpdateDocumentState(DocumentState documentState, CancellationToken cancellationToken)
        {
            DocumentStateTransaction documentStateTransaction = await AddTransaction(documentState, cancellationToken);

            documentState.CurrentDocumentStateId = documentStateTransaction.Id;

            _dbContext.DocumentStates.Update(documentState);

            return documentStateTransaction;
        }

        private async Task<DocumentStateTransaction> AddTransaction(DocumentState documentState, CancellationToken cancellationToken)
        {
            DocumentStateTransaction documentStateTransaction = _mapper.Map<DocumentStateTransaction>(documentState);

            await _dbContext.DocumentStateTransactions.AddAsync(documentStateTransaction);

            await _dbContext.SaveChangesAsync(cancellationToken);

            return documentStateTransaction;
        }
        
        public Employee AssignEmployee(int routeStageId)
        {
            Employee employee = (from employees in _dbContext.EmployeeRouteStages.Include(i => i.Employee).Where(w => w.RouteStageId == routeStageId)
                                 join activeDocuments in _dbContext.DocumentStates.Where(w => w.State != State.Finished) on employees.EmployeeId equals activeDocuments.EmployeeId into lj
                                 from document in lj.DefaultIfEmpty()
                                 select new
                                 {
                                     employees.Employee,
                                     document
                                 }
                ).ToList()
                .GroupBy(g => g.Employee)
                .Select(s => new { s.Key, Count = s.Count(c => c.document == null ? false : true) })
                .OrderBy(o => o.Count)
                .Select(s => s.Key)
                .FirstOrDefault();

            return employee;
        }

        public RouteStage GetNextRouteStage(int routeId, int currentRouteStageOrder)
        {
            RouteStage routeStageNext = GetNextRouteStageOrNull(routeId, currentRouteStageOrder);

            if (routeStageNext == null)
            {
                int routeStageOrderNext = currentRouteStageOrder + 1;
                throw new NullReferenceException($"RouteStage with routeId = {routeId} and RouteStageOrder = {routeStageOrderNext} does not exist");
            }

            return routeStageNext;
        }

        public RouteStage GetNextRouteStageOrNull(int routeId, int currentRouteStageOrder)
        {
            int routeStageOrderNext = currentRouteStageOrder + 1;
            RouteStage routeStageNext = _dbContext.Routes
                .Where((r) => r.Id == routeId)
                .Include(ti => ti.RouteBodies)
                .ThenInclude(rb => rb.RouteStage)
                .First()
                .RouteBodies
                .Where(w => w.RouteStageOrder == routeStageOrderNext)
                .Select(s => s.RouteStage)
                .FirstOrDefault();

            return routeStageNext;
        }

        public DocumentStateTransaction GetLastDocumentStateTransaction(int documentId, int routeStageOrder)
        {
            DocumentStateTransaction documentStateTransaction = _dbContext.DocumentStateTransactions
                .Where(ds => ds.DocumentId == documentId && ds.RouteStageOrder == routeStageOrder && ds.Result != Result.Reject && ds.Result != Result.OnHold)
                .Include(ds => ds.RouteStageNext)
                .OrderByDescending(ds => ds.Id)
                .FirstOrDefault();

            return documentStateTransaction;
        }
    }
}
