﻿using System.Threading;
using System.Threading.Tasks;
using DocumentFlow.Entities.Models;

namespace DocumentFlow.UseCases.Managers.Interfaces
{
    public interface IDocumentManager
    {
        Task<DocumentState> CreateDocumentState(Document document, CancellationToken cancellationToken);
        Task<DocumentStateTransaction> UpdateDocumentState (DocumentState documentState, CancellationToken cancellationToken);
        Employee AssignEmployee(int routeStageId);
        RouteStage GetNextRouteStage(int routeId, int currentRouteStageOrder);
        RouteStage GetNextRouteStageOrNull(int routeId, int currentRouteStageOrder);
        DocumentStateTransaction GetLastDocumentStateTransaction(int documentId, int routeStageOrder);
    }
}