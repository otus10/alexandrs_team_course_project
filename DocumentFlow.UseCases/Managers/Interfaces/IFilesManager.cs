﻿using System.Threading.Tasks;
using DocumentFlow.Entities.Models;
using Microsoft.AspNetCore.Http;

namespace DocumentFlow.UseCases.Managers.Interfaces
{
    public interface IFilesManager
    {
        Task UploadFiles(Document document, IFormFile[] uploads);
    }
}