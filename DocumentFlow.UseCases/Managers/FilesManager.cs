﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DocumentFlow.DataAccess.Interfaces.DataAccess;
using DocumentFlow.Entities.Models;
using DocumentFlow.UseCases.Managers.Interfaces;
using DocumentFlow.UseCases.SettingsModels;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace DocumentFlow.UseCases.Managers
{
    public class FilesManager : IFilesManager
    {
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IOptions<UploadingFilesSettings> _uploadingFilesSettings;

        public FilesManager(IDbContext dbContext, IMapper mapper, IOptions<UploadingFilesSettings> uploadingFilesSettings)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _uploadingFilesSettings = uploadingFilesSettings;
        }

        public async Task UploadFiles(Document document, IFormFile[] uploads)
        {
            if (CheckDuplicateFiles(uploads, out List<string> fileNamesDuplicates))
            {
                throw new InvalidOperationException(
                    $"Trying to download duplicate files: {string.Join(", ", fileNamesDuplicates)}");
            }

            List<Thread> threads = new();
            string dir = GetAndInitDirFiles(document.Id);

            foreach (IFormFile formFile in uploads)
            {
                Attachment attachment = new()
                {
                    FileName = formFile.FileName,
                    FilePath = $"{dir}\\{formFile.FileName}",
                };
                CheckAndModifyAttachment(attachment);

                UploadedFile uploadedFile = new()
                {
                    FormFile = formFile,
                    AttachmentInfo = attachment
                };
                Thread thread = new(UploadFile);
                threads.Add(thread);
                thread.Start(uploadedFile);
                document.Attachment.Add(attachment);
            }

            foreach (Thread thread in threads)
            {
                thread.Join();
            }
        }

        private bool CheckDuplicateFiles(IFormFile[] uploads, out List<string> fileNamesDuplicates)
        {
            var fileNamesDuplicatesSet = new HashSet<string>();
            var fileNames = uploads.Select(x => x.FileName).ToList();
            foreach (var f in fileNames.Where(f => fileNames.FindAll(x => x == f).Count > 1))
            {
                fileNamesDuplicatesSet.Add(f);
            }

            if (fileNamesDuplicatesSet.Count > 0)
            {
                fileNamesDuplicates = fileNamesDuplicatesSet.ToList();
                fileNamesDuplicates.Sort();
                return true;
            }

            fileNamesDuplicates = null;
            return false;
        }

        private string GetAndInitDirFiles(int documentId)
        {
            string dirPath = _uploadingFilesSettings.Value.Directory;
            DirectoryInfo dir = new(Path.Combine(dirPath, documentId.ToString()));
            if (!dir.Exists)
            {
                dir.Create();
            }

            return dir.FullName;
        }

        private void CheckAndModifyAttachment(Attachment attachment)
        {
            FileInfo fileInfo = new(attachment.FilePath);
            if (!fileInfo.Exists)
            {
                return;
            }

            string fileNameMod = attachment.FileName.Replace(fileInfo.Extension, $"_{fileInfo.Extension}");
            attachment.FilePath = attachment.FilePath.Replace(fileInfo.Name, fileNameMod);
            attachment.FileName = fileNameMod;
            CheckAndModifyAttachment(attachment);
        }

        private void UploadFile(object uploadedFileObj)
        {
            var uploadedFile = (UploadedFile)uploadedFileObj;
            using FileStream fileStream = new(uploadedFile.AttachmentInfo.FilePath, FileMode.Create);
            uploadedFile.FormFile.CopyTo(fileStream);
            fileStream.Flush();
        }

        private class UploadedFile
        {
            public IFormFile FormFile { get; set; }
            public Attachment AttachmentInfo { get; set; }
        }
    }
}
