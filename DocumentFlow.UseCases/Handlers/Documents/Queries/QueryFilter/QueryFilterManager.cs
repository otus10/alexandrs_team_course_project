﻿using System.Collections.Generic;
using System.Linq;
using DocumentFlow.Entities.Models;

namespace DocumentFlow.UseCases.Handlers.Documents.Queries.QueryFilter
{
    public class QueryFilterManager : IQueryFilter
    {
        private readonly List<IQueryFilter> _queryFilters = new();

        public IQueryable<DocumentState> Filter(IQueryable<DocumentState> documentStates)
        {
            foreach (IQueryFilter queryFilter in _queryFilters)
            {
                documentStates = queryFilter.Filter(documentStates);
            }

            return documentStates;
        }

        public QueryFilterManager AddQueryFilter(QueryFilterBase queryFilterPrototype, int? filterValue)
        {
            if (filterValue.HasValue)
            {
                _queryFilters.Add(queryFilterPrototype.CreateFilter(filterValue.Value));
            }

            return this;
        }
    }
}
