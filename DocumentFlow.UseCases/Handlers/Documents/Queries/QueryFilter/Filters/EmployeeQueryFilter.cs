﻿using System.Linq;
using DocumentFlow.Entities.Models;

namespace DocumentFlow.UseCases.Handlers.Documents.Queries.QueryFilter.Filters
{
    public class EmployeeQueryFilter : QueryFilterBase
    {
        public static EmployeeQueryFilter Prototype { get; } = new();

        public EmployeeQueryFilter()
        {
        }

        public EmployeeQueryFilter(int filterValue) : base(filterValue)
        {
        }

        public override IQueryable<DocumentState> Filter(IQueryable<DocumentState> documentStates)
        {
            return documentStates.Where(x => x.EmployeeId == FilterValue);
        }

        public override QueryFilterBase CreateFilter(int filterValue)
        {
            return new EmployeeQueryFilter(filterValue);
        }
    }
}
