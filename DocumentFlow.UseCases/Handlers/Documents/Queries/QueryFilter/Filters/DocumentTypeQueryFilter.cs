﻿using System.Linq;
using DocumentFlow.Entities.Models;

namespace DocumentFlow.UseCases.Handlers.Documents.Queries.QueryFilter.Filters
{
    public class DocumentTypeQueryFilter : QueryFilterBase
    {
        public static DocumentTypeQueryFilter Prototype { get; } = new();

        public DocumentTypeQueryFilter()
        {
        }

        public DocumentTypeQueryFilter(int filterValue) : base(filterValue)
        {
        }

        public override IQueryable<DocumentState> Filter(IQueryable<DocumentState> documentStates)
        {
            return documentStates.Where(x => x.Document.DocumentTemplate.DocumentTypeId == FilterValue);
        }

        public override QueryFilterBase CreateFilter(int filterValue)
        {
            return new DocumentTypeQueryFilter(filterValue);
        }
    }
}
