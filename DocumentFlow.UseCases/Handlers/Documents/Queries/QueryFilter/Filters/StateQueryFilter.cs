﻿using System.Linq;
using DocumentFlow.Entities.Models;

namespace DocumentFlow.UseCases.Handlers.Documents.Queries.QueryFilter.Filters
{
    public class StateQueryFilter : QueryFilterBase
    {
        public static StateQueryFilter Prototype { get; } = new();

        public StateQueryFilter()
        {
        }

        public StateQueryFilter(int filterValue) : base(filterValue)
        {
        }

        public override IQueryable<DocumentState> Filter(IQueryable<DocumentState> documentStates)
        {
            return documentStates.Where(x => x.State == (State)FilterValue);
        }

        public override QueryFilterBase CreateFilter(int filterValue)
        {
            return new StateQueryFilter(filterValue);
        }
    }
}
