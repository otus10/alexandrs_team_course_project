﻿using System.Linq;
using DocumentFlow.Entities.Models;

namespace DocumentFlow.UseCases.Handlers.Documents.Queries.QueryFilter.Filters
{
    public class ResultQueryFilter : QueryFilterBase
    {
        public static ResultQueryFilter Prototype { get; } = new();

        public ResultQueryFilter()
        {
        }

        public ResultQueryFilter(int filterValue) : base(filterValue)
        {
        }

        public override IQueryable<DocumentState> Filter(IQueryable<DocumentState> documentStates)
        {
            return documentStates.Where(x => x.Result == (Result)FilterValue);
        }
        
        public override QueryFilterBase CreateFilter(int filterValue)
        {
            return new ResultQueryFilter(filterValue);
        }
    }
}
