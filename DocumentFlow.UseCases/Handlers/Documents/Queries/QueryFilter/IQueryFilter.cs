﻿using System.Linq;
using DocumentFlow.Entities.Models;

namespace DocumentFlow.UseCases.Handlers.Documents.Queries.QueryFilter
{
    public interface IQueryFilter
    {
        IQueryable<DocumentState> Filter(IQueryable<DocumentState> documentStates);
    }
}
