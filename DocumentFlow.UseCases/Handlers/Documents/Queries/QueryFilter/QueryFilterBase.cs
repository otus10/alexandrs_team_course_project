﻿using System.Linq;
using DocumentFlow.Entities.Models;

namespace DocumentFlow.UseCases.Handlers.Documents.Queries.QueryFilter
{
    public abstract class QueryFilterBase : IQueryFilter
    {
        public int FilterValue { get; }

        protected QueryFilterBase()
        {
        }

        protected QueryFilterBase(int filterValue)
        {
            FilterValue = filterValue;
        }

        public abstract IQueryable<DocumentState> Filter(IQueryable<DocumentState> documentStates);

        public abstract QueryFilterBase CreateFilter(int filterValue);
    }
}
