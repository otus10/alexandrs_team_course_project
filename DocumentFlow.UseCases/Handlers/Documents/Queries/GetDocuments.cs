﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DocumentFlow.DataAccess.Interfaces.DataAccess;
using DocumentFlow.Entities.Models;
using DocumentFlow.UseCases.Handlers.Documents.Dto;
using DocumentFlow.UseCases.Handlers.Documents.Queries.QueryFilter;
using DocumentFlow.UseCases.Handlers.Documents.Queries.QueryFilter.Filters;
using DocumentFlow.UserCases.Exceptions;
using DocumentFlow.UserCases.Handlers.Documents.Dto;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace DocumentFlow.UseCases.Handlers.Documents.Queries
{
    public static class GetDocuments
    {
        public class Request : GetDocumentsDto, IRequest<List<DocumentDto>>
        {
        }

        public class Handler : IRequestHandler<Request, List<DocumentDto>>
        {
            private readonly IDbContext _dbContext;
            private readonly IMapper _mapper;

            public Handler(IDbContext dbContext, IMapper mapper)
            {
                _dbContext = dbContext;
                _mapper = mapper;
            }

            public async Task<List<DocumentDto>> Handle(Request request, CancellationToken cancellationToken)
            {

                IQueryable<DocumentState> order = _dbContext.DocumentStates
                    .Include(d => d.Document)
                    .ThenInclude(i => i.DocumentTemplate)
                    .ThenInclude(i => i.DocumentType)
                    .Include(rs => rs.RouteStageNext)
                    .Include(empl => empl.Employee)
                    .Include(empl => empl.InitiatorDocumentState).ThenInclude(empl => empl.Employee)
                    .Include(empl => empl.PreviousDocumentState).ThenInclude(empl => empl.Employee)
                    .AsNoTracking();

                if (order == null)
                {
                    throw new EntityNotFoundException();
                }

                order = Filter(order, request);

                List<DocumentDto> result = order
                    .OrderBy(x => x.DocumentId)
                    .Select(x => _mapper.Map<DocumentDto>(x))
                    .ToList();

                return result;
            }

            private IQueryable<DocumentState> Filter(IQueryable<DocumentState> order, Request request)
            {
                return new QueryFilterManager()
                    .AddQueryFilter(StateQueryFilter.Prototype, request.DocumentStateId)
                    .AddQueryFilter(ResultQueryFilter.Prototype, request.Result)
                    .AddQueryFilter(EmployeeQueryFilter.Prototype, request.EmployeeId)
                    .AddQueryFilter(DocumentTypeQueryFilter.Prototype, request.DocumentTypeId)
                    .Filter(order);
            }
        }
    }
}