﻿using AutoMapper;
using DocumentFlow.DataAccess.Interfaces.DataAccess;
using DocumentFlow.Entities.Models;
using DocumentFlow.UseCases.Handlers.Documents.Dto;
using DocumentFlow.UserCases.Exceptions;
using DocumentFlow.UserCases.Handlers.Documents.Dto;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DocumentFlow.UserCases.Handlers.Documents.Queries.GetDocumentType
{
   public static class GetDocumentType
   {
      public class Request : GetDocumentTypeDto, IRequest<DocumentTypeDto>
      { 
      }

      public class Handler : IRequestHandler<GetDocumentType.Request, DocumentTypeDto>
      {
         private readonly IDbContext _dbContext;
         private readonly IMapper _mapper;

         public Handler(IDbContext dbContext, IMapper mapper)
         {
            _dbContext = dbContext;
            _mapper = mapper;
         }

         public async Task<DocumentTypeDto> Handle(GetDocumentType.Request request, CancellationToken cancellationToken)
         {
            DocumentType order = await _dbContext.DocumentTypes.AsNoTracking()
                .SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken: cancellationToken);

            if (order == null) throw new EntityNotFoundException();

            DocumentTypeDto result = _mapper.Map<DocumentTypeDto>(order);
            return result;
         }
      }
   }
}
