﻿using DocumentFlow.Entities.Models;
using DocumentFlow.UseCases.Handlers.Documents.Dto;
using DocumentFlow.UserCases.Handlers.Documents.Dto;
using AutoMapper;

namespace DocumentFlow.UserCases.Handlers.Documents.Mappings
{
    public class DocumentTypeAutoMapperProfile : Profile
    {
        public DocumentTypeAutoMapperProfile()
        {
            CreateMap<DocumentType, DocumentTypeDto>();
            CreateMap<CreateDocumentTypeDto, DocumentType>();
            CreateMap<AttachmentDto, Attachment>().ReverseMap();
            CreateMap<CreateDocumentDto, Document>();
            CreateMap<StartDocumentRouteDto, DocumentState>();
            CreateMap<DocumentState, DocumentStateTransaction>().ReverseMap();
            CreateMap<DocumentState, DocumentDto>()
                .ForMember(m => m.DocumentName, options => options.MapFrom(src => src.Document.Name))
                .ForMember(m => m.AssignEmployee, options => options.MapFrom (src => src.Employee.FullName))
                .ForMember(m => m.LastAction, options => options.MapFrom(src => src.Result))
                .ForMember(m => m.RouteStageNext, options => options.MapFrom(src => src.RouteStageNext.Description))
                .ForMember(m => m.Initiator, options => options.MapFrom(src => src.InitiatorDocumentState.Employee.FullName))
                .ForMember(m => m.PreviousEmployee, options => options.MapFrom(src => src.PreviousDocumentState.Employee.FullName))
                .ForMember(m => m.State, options => options.MapFrom(src => src.State.ToString()))
                ;
        }
    }
}
