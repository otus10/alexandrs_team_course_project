﻿using AutoMapper;
using DocumentFlow.DataAccess.Interfaces.DataAccess;
using DocumentFlow.Entities.Models;
using Microsoft.EntityFrameworkCore;
using DocumentFlow.UseCases.Handlers.Documents.Dto;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DocumentFlow.UseCases.Managers.Interfaces;
using Microsoft.EntityFrameworkCore.Storage;

namespace DocumentFlow.UseCases.Handlers.Documents.Commands
{
    public static class StartDocumentRoute

    {
        public class Command : StartDocumentRouteDto, IRequest
        {
        }

        public class Handler : AsyncRequestHandler<Command>
        {
            private readonly IDbContext _dbContext;
            private readonly IMapper _mapper;
            private readonly IDocumentManager _documentManager;

            public Handler(IDbContext dbContext, IMapper mapper, IDocumentManager documentManager)
            {
                _dbContext = dbContext;
                _mapper = mapper;
                _documentManager = documentManager;
            }

            protected override async Task Handle(Command request, CancellationToken cancellationToken)
            {
                DocumentState documentState = _dbContext.DocumentStates
                    .Include(i => i.Document)
                    .ThenInclude(i => i.DocumentTemplate)
                    .Include(i => i.RouteStageNext)
                    .Where((ds) => ds.DocumentId == request.DocumentId)
                    .FirstOrDefault();

                if (documentState == null)
                {
                    throw new ArgumentOutOfRangeException($"DocumentState with DocumentId = {request.DocumentId} does not exist");
                }

                if (documentState.State == State.Created)
                {
                    SetFieldsForFirstStart(documentState);
                }
                else if (documentState.State == State.InRouting && (documentState.Result == Result.Reject || documentState.Result == Result.Initiate) && documentState.RouteStageOrder == null)
                {
                    SetFieldsForRestart(documentState);
                }
                else
                {
                    throw new InvalidOperationException($"It is impossible to start the route of the document with id = {documentState.DocumentId}");
                }

                documentState.DateUpdated = DateTime.Now;
                documentState.PreviousDocumentStateId = documentState.CurrentDocumentStateId;
                documentState.Comment = request.Comment;
                documentState.Result = Result.StartRoute;


                using (IDbContextTransaction dbContextTransaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    _ = await _documentManager.UpdateDocumentState(documentState, cancellationToken);

                    await _dbContext.SaveChangesAsync(cancellationToken);

                    await dbContextTransaction.CommitAsync();
                }
            }

            private void SetFieldsForFirstStart(DocumentState documentState)
            {
                documentState.RouteStageOrder = 1;
                documentState.State = State.InRouting;
                documentState.RouteStageNext = _documentManager.GetNextRouteStage(documentState.Document.DocumentTemplate.RouteId, documentState.RouteStageOrder.Value);
                documentState.Employee = _documentManager.AssignEmployee(documentState.RouteStageNextId.Value);
            }

            private void SetFieldsForRestart(DocumentState documentState)
            {
                documentState.RouteStageOrder = 1;
                DocumentStateTransaction lastTransaction = _documentManager.GetLastDocumentStateTransaction(documentState.DocumentId, documentState.RouteStageOrder.Value);
                documentState.EmployeeId = lastTransaction.EmployeeId;
                documentState.RouteStageNextId = lastTransaction.RouteStageNextId;
            }
        }
    }
}
