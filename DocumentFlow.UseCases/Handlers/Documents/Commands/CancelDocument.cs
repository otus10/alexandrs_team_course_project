﻿using AutoMapper;
using DocumentFlow.DataAccess.Interfaces.DataAccess;
using DocumentFlow.Entities.Models;
using DocumentFlow.UseCases.Handlers.Documents.Dto;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DocumentFlow.UseCases.Managers.Interfaces;
using Microsoft.EntityFrameworkCore.Storage;

namespace DocumentFlow.UseCases.Handlers.Documents.Commands
{
    public static class CancelDocument
    {
        public class Command : CancelDocumentDto, IRequest
        {
        }

        public class Handler : AsyncRequestHandler<Command>
        {
            private readonly IDbContext _dbContext;
            private readonly IMapper _mapper;
            private readonly IDocumentManager _documentManager;

            public Handler(IDbContext dbContext, IMapper mapper, IDocumentManager documentManager)
            {
                _dbContext = dbContext;
                _mapper = mapper;
                _documentManager = documentManager;
            }

            protected override async Task Handle(Command request, CancellationToken cancellationToken)
            {
                DocumentState documentState = _dbContext.DocumentStates
                    .Where((ds) => ds.DocumentId == request.DocumentId)
                    .FirstOrDefault();

                if (documentState == null)
                {
                    throw new ArgumentOutOfRangeException($"DocumentState with DocumentId = {request.DocumentId} does not exist");
                }

                if (documentState.State == State.Created || documentState.State == State.Finished)
                {
                    throw new InvalidOperationException($"It is impossible to cancel the document with DocumentId = {request.DocumentId}, because his status = {documentState.State}");
                }

                documentState.DateUpdated = DateTime.Now;
                documentState.Result = Result.Cancel;
                documentState.PreviousDocumentStateId = documentState.CurrentDocumentStateId;
                documentState.Comment = request.Comment;
                documentState.RouteStageNextId = null;
                documentState.State = State.Finished;

                using (IDbContextTransaction dbContextTransaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    _ = await _documentManager.UpdateDocumentState(documentState, cancellationToken);

                    await _dbContext.SaveChangesAsync(cancellationToken);

                    await dbContextTransaction.CommitAsync();
                }
            }
        }
    }
}
