﻿using System;
using System.Linq;
using AutoMapper;
using DocumentFlow.DataAccess.Interfaces.DataAccess;
using DocumentFlow.UseCases.Handlers.Documents.Dto;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using DocumentFlow.Entities.Models;
using DocumentFlow.UseCases.Managers.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DocumentFlow.UseCases.Handlers.Documents.Commands
{
    public static class AddFiles

    {
        public class Command : AddFilesDto, IRequest
        {
        }

        public class Handler : AsyncRequestHandler<AddFiles.Command>
        {
            private readonly IDbContext _dbContext;
            private readonly IMapper _mapper;
            private readonly IFilesManager _filesManager;

            public Handler(IDbContext dbContext, IMapper mapper, IFilesManager filesManager)
            {
                _dbContext = dbContext;
                _mapper = mapper;
                _filesManager = filesManager;
            }

            protected override async Task Handle(AddFiles.Command request, CancellationToken cancellationToken)
            {
                Document document = _dbContext.Documents
                    .Include(i => i.Attachment)
                    .Where((d) => d.Id == request.DocumentId)
                    .FirstOrDefault();

                if (document == null)
                {
                    throw new ArgumentNullException($"Document with Id = {request.DocumentId} does not exist");
                }

                await _filesManager.UploadFiles(document, request.Uploads);

                await _dbContext.SaveChangesAsync(cancellationToken);
            }
        }
    }
}