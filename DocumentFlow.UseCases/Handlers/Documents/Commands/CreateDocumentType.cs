﻿using AutoMapper;
using DocumentFlow.DataAccess.Interfaces.DataAccess;
using DocumentFlow.Entities.Models;
using DocumentFlow.UseCases.Handlers.Documents.Dto;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DocumentFlow.UseCases.Handlers.Documents.Commands
{
   public static class CreateDocumentType

   {
      public class Command : CreateDocumentTypeDto, IRequest
      {
      }

      public class Handler : AsyncRequestHandler<CreateDocumentType.Command>
      {
         private readonly IDbContext _dbContext;
         private readonly IMapper _mapper;

         public Handler(IDbContext dbContext, IMapper mapper)
         {
            _dbContext = dbContext;
            _mapper = mapper;
         }

         protected override async Task Handle(CreateDocumentType.Command request, CancellationToken cancellationToken)
         {
            DocumentType documentType = _mapper.Map<DocumentType>(request);
            documentType.DateCreated = DateTime.Now;

            _dbContext.DocumentTypes.Add(documentType);

            await _dbContext.SaveChangesAsync(cancellationToken);
         }
      }
   }
}
