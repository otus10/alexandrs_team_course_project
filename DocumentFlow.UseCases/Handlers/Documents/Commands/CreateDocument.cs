﻿using AutoMapper;
using DocumentFlow.DataAccess.Interfaces.DataAccess;
using DocumentFlow.Entities.Models;
using DocumentFlow.UseCases.Handlers.Documents.Dto;
using MediatR;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DocumentFlow.UseCases.Managers.Interfaces;

namespace DocumentFlow.UseCases.Handlers.Documents.Commands
{
    public static class CreateDocument

    {
        public class Command : CreateDocumentDto, IRequest
        {
        }

        public class Handler : AsyncRequestHandler<CreateDocument.Command>
        {
            private readonly IDbContext _dbContext;
            private readonly IMapper _mapper;
            private readonly IDocumentManager _documentManager;
            private readonly IFilesManager _filesManager;

            public Handler(IDbContext dbContext, IMapper mapper, IDocumentManager documentManager, IFilesManager filesManager)
            {
                _dbContext = dbContext;
                _mapper = mapper;
                _documentManager = documentManager;
                _filesManager = filesManager;
            }

            protected override async Task Handle(CreateDocument.Command request, CancellationToken cancellationToken)
            {
                Document document = _mapper.Map<Document>(request);
                document.DateCreated = DateTime.Now;
                document.InitiatorId = 1;
                document.Attachment = new List<Attachment>();

                using (IDbContextTransaction dbContextTransaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    await _dbContext.Documents.AddAsync(document);
                    await _dbContext.SaveChangesAsync(cancellationToken);
                    _ = await _documentManager.CreateDocumentState(document, cancellationToken);

                    if (request.Uploads != null)
                    {
                        await _filesManager.UploadFiles(document, request.Uploads);
                    }

                    await _dbContext.SaveChangesAsync(cancellationToken);
                    await dbContextTransaction.CommitAsync();
                }
            }
        }
    }
}