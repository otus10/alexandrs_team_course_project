﻿using AutoMapper;
using DocumentFlow.DataAccess.Interfaces.DataAccess;
using DocumentFlow.Entities.Models;
using Microsoft.EntityFrameworkCore;
using DocumentFlow.UseCases.Handlers.Documents.Dto;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DocumentFlow.UseCases.Managers.Interfaces;
using Microsoft.EntityFrameworkCore.Storage;

namespace DocumentFlow.UseCases.Handlers.Documents.Commands
{
    public static class ApproveDocument

    {
        public class Command : ApproveDocumentDto, IRequest
        {
        }

        public class Handler : AsyncRequestHandler<Command>
        {
            private readonly IDbContext _dbContext;
            private readonly IMapper _mapper;
            private readonly IDocumentManager _documentManager;

            public Handler(IDbContext dbContext, IMapper mapper, IDocumentManager documentManager)
            {
                _dbContext = dbContext;
                _mapper = mapper;
                _documentManager = documentManager;
            }

            protected override async Task Handle(Command request, CancellationToken cancellationToken)
            {
                DocumentState documentState = _dbContext.DocumentStates
                    .Include(i => i.Document)
                    .ThenInclude(i => i.DocumentTemplate)
                    .Include(i => i.RouteStageNext)
                    .Where((ds) => ds.DocumentId == request.DocumentId)
                    .FirstOrDefault();

                if (documentState == null)
                {
                    throw new ArgumentOutOfRangeException($"DocumentState with DocumentId = {request.DocumentId} does not exist");
                }
                if (documentState.State == State.Finished)
                {
                    throw new InvalidOperationException($"it is impossible to approve the document with DocumentId = {request.DocumentId}, because his status = {documentState.State}");
                }

                documentState.DateUpdated = DateTime.Now;
                documentState.Result = Result.Approve;
                documentState.PreviousDocumentStateId = documentState.CurrentDocumentStateId;
                documentState.Comment = request.Comment;
                if (documentState.RouteStageNextId != null)
                {
                    SetEmployeeAndRouteStageNext(documentState);
                }
                else
                {
                    documentState.State = State.Finished;
                }

                using (IDbContextTransaction dbContextTransaction = await _dbContext.Database.BeginTransactionAsync())
                {
                    _ = await _documentManager.UpdateDocumentState(documentState, cancellationToken);

                    await _dbContext.SaveChangesAsync(cancellationToken);

                    await dbContextTransaction.CommitAsync();
                }
            }

            private void SetEmployeeAndRouteStageNext(DocumentState documentState)
            {
                documentState.RouteStageOrder++;
                DocumentStateTransaction lastTransaction = _documentManager.GetLastDocumentStateTransaction(documentState.DocumentId, documentState.RouteStageOrder.Value);
                if (lastTransaction == null)
                {
                    SetFieldsForFirstApprove(documentState);
                }
                else
                {
                    SetFieldsForReApprove(documentState, lastTransaction);
                }
            }

            private void SetFieldsForFirstApprove(DocumentState documentState)
            {
                documentState.Employee = _documentManager.AssignEmployee(documentState.RouteStageNextId.Value);
                documentState.RouteStageNext = _documentManager.GetNextRouteStageOrNull(documentState.Document.DocumentTemplate.RouteId, documentState.RouteStageOrder.Value);
                if (documentState.RouteStageNext == null)
                {
                    documentState.RouteStageNextId = null;
                }
            }

            private void SetFieldsForReApprove(DocumentState documentState, DocumentStateTransaction lastTransaction)
            {
                documentState.EmployeeId = lastTransaction.EmployeeId;
                documentState.RouteStageNext = lastTransaction.RouteStageNext;
            }
        }
    }
}
