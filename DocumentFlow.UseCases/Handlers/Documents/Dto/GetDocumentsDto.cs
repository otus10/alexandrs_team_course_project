﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.UseCases.Handlers.Documents.Dto
{
    public class GetDocumentsDto
    {
        public int? EmployeeId { get; set; }
        public int? Result { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? DocumentStateId { get; set; }
    }
}