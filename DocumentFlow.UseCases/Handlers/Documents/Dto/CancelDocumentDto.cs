﻿using Microsoft.AspNetCore.Mvc;

namespace DocumentFlow.UseCases.Handlers.Documents.Dto
{
    public class CancelDocumentDto
    {
        public int DocumentId { get; set; }

        [FromBody]
        public string Comment { get; set; }
    }
}
