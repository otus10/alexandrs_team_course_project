﻿using Microsoft.AspNetCore.Http;

namespace DocumentFlow.UseCases.Handlers.Documents.Dto
{
    public class CreateDocumentDto
    {
        public int DocumentTemplateId { get; set; }
        public string Name { get; set; }
        public IFormFile[] Uploads { get; set; }
    }
}