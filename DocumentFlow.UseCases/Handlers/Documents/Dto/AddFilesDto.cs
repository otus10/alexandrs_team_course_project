﻿using Microsoft.AspNetCore.Http;

namespace DocumentFlow.UseCases.Handlers.Documents.Dto
{
    public class AddFilesDto
    {
        public int DocumentId { get; set; }
        public IFormFile[] Uploads { get; set; }
    }
}