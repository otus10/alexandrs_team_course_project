﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.UseCases.Handlers.Documents.Dto
{
   public class CreateDocumentTypeDto
   {
      public int Id { get; set; }
      public DateTime DateCreated { get; set; }
      public DateTime? DateUpdated { get; set; }
      public string Code { get; set; }
      public string NameEn { get; set; }
   }
}
