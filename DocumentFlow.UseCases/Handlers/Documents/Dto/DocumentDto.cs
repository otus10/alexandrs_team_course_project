﻿using System;
using DocumentFlow.Entities.Models;

namespace DocumentFlow.UserCases.Handlers.Documents.Dto
{
    public class DocumentDto
    {
        public int DocumentId { get; set; }
        public string DocumentName { get; set; }
        public string State { get; set; }
        public string AssignEmployee { get; set; }
        public int? RouteStageOrder { get; set; }
        public string RouteStageNext { get; set; }
        public string Initiator { get; set; }
        public DateTime DateCreated { get; set; } = DateTime.Now;
        public string PreviousEmployee { get; set; }
        public DateTime? DateUpdated { get; set; }
        public string LastAction { get; set; }
        public string Comment { get; set; }
        
    }
}