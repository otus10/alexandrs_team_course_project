﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.UseCases.Handlers.Documents.Dto
{
    public class AttachmentDto
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }
}
