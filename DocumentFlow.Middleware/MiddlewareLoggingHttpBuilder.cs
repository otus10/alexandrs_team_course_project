﻿using DocumentFlow.Utils.Modules;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;


namespace DocumentFlow.MiddlewarLoggingHttp
{
   public static class MiddlewareLoggingHttpModuleBuilder
   {
		public static IApplicationBuilder UseMiddlewareLoggingHttpModule(this IApplicationBuilder builder)
		{
			return builder.UseMiddleware<MiddlewareLoggingHttpModule>();
		}
	}
}
