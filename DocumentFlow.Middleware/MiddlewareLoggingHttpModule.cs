﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace DocumentFlow.MiddlewarLoggingHttp
{
    public class MiddlewareLoggingHttpModule
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        public MiddlewareLoggingHttpModule(RequestDelegate next, ILogger<MiddlewareLoggingHttpModule> logger)
        {
            this._next = next;
            this._logger = logger;
        }
        public async Task Invoke(HttpContext context)
        {
            _logger.LogInformation("before request");
            await _next.Invoke(context);
            _logger.LogInformation("after request");
        }
    }
}
