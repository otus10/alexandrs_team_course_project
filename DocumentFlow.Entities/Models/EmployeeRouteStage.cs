﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.Entities.Models
{
    public class EmployeeRouteStage
    {
        public int EmployeeId { get; set; }

        public Employee Employee { get; set; }

        public int RouteStageId { get; set; }

        public RouteStage RouteStage { get; set; }
    }
}
