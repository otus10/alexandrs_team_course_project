﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.Entities.Models
{
    public enum State
    {
        Created = 0,
        InRouting = 1,
        Finished = 2
    }
}
