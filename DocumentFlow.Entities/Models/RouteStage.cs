﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.Entities.Models
{
    public class RouteStage : BaseEntity
    {
        public string Description { get; set; }

        public IList<EmployeeRouteStage> EmployeeRouteStages { get; set; }
    }
}
