﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.Entities.Models
{
    public class Employee : BaseEntity
    {
        public string FullName { get; set; }

        public JobTitle JobTitle { get; set; }

        public IList<EmployeeRouteStage> EmployeeRouteStages { get; set; }
    }
}
