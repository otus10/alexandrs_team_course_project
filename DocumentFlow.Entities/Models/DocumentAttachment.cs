﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.Entities.Models
{
    public class DocumentAttachment : BaseEntity
    {
        public Document Document { get; set; }

        public int DocumentId { get; set; }

        public Attachment Attachment { get; set; }

        public int AttachmentId { get; set; }
    }
}
