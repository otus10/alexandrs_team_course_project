﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.Entities.Models
{
    public enum JobTitle
    {
        Developer,
        TeamLead,
        ProjectManager,
        ChiefDevelopmentDepartment,
        HRManager,
        ChiefHR,
        Accountant,
        SeniorAccountant,
        ChiefAccountant,
        FinancialDirector,
        GeneralDirector
    }
}
