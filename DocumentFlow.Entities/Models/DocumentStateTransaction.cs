﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.Entities.Models
{
    public class DocumentStateTransaction : BaseEntity
    {
        public Document Document { get; set; }
        public int DocumentId { get; set; }
        public Employee Employee { get; set; }
        public int EmployeeId { get; set; }
        public Result Result { get; set; }
        public int? RouteStageNextId { get; set; }
        public RouteStage RouteStageNext { get; set; }
        public DocumentStateTransaction InitiatorDocumentState { get; set; }
        public int? InitiatorDocumentStateId { get; set; }
        public DocumentStateTransaction PreviousDocumentState { get; set; }
        public int? PreviousDocumentStateId { get; set; }
        public string Comment { get; set; }
        public int? RouteStageOrder { get; set; }
        public State State { get; set; }
    }
}
