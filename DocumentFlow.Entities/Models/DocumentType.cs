﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.Entities.Models
{
   public class DocumentType : BaseEntity
   {
      public string Code { get; set; }

      public string NameEn { get; set; }

      public IList<DocumentTemplate> DocumentTemplates { get; set; }
   }
}
