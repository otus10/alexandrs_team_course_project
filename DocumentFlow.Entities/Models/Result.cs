﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.Entities.Models
{
    public enum Result
    {
        Initiate = 0,
        Approve = 1,
        Reject = 2,
        Cancel = 3,
        OnHold = 4,
        StartRoute = 5
    }
}
