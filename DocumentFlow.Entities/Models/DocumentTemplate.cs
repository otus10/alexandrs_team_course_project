﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.Entities.Models
{
    public class DocumentTemplate : BaseEntity
    {
        public int RouteId { get; set; }

        public Route Route { get; set; }

        public int DocumentTypeId { get; set; }

        public DocumentType DocumentType { get; set; }
    }
}
