﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.Entities.Models
{
    public class DocumentState
    {
        public Document Document { get; set; }
        [Key]
        public int DocumentId { get; set; }
        public Employee Employee { get; set; }
        public int EmployeeId { get; set; }
        public Result Result { get; set; }
        public RouteStage RouteStageNext { get; set; }
        public int? RouteStageNextId { get; set; }
        public DocumentStateTransaction InitiatorDocumentState { get; set; }
        public int InitiatorDocumentStateId { get; set; }
        public DocumentStateTransaction PreviousDocumentState { get; set; }
        public int? PreviousDocumentStateId { get; set; }
        public string Comment { get; set; }
        public DateTime DateCreated { get; set; } = DateTime.Now;
        public DateTime? DateUpdated { get; set; }
        public DocumentStateTransaction CurrentDocumentState { get; set; }
        public int? CurrentDocumentStateId { get; set; }
        public int? RouteStageOrder { get; set; }
        public State State { get; set; }
    }
}
