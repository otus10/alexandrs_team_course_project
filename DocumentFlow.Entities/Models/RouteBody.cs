﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.Entities.Models
{
    public class RouteBody : BaseEntity
    {
        public Route Route { get; set; }
        public int RouteId { get; set; }
        public RouteStage RouteStage { get; set; }
        public int RouteStageId { get; set; }
        public int RouteStageOrder { get; set; }
    }
}
