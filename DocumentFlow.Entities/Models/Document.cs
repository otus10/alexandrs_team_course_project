﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.Entities.Models
{
    public class Document : BaseEntity
    {
        public DocumentTemplate DocumentTemplate { get; set; }
        public int DocumentTemplateId { get; set; }

        public string Name { get; set; }
        public Employee Initiator { get; set; }
        public int InitiatorId { get; set; }
        public IList<Attachment> Attachment { get; set; }

    }
}
