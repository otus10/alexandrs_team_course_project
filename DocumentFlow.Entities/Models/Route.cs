﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.Entities.Models
{
    public class Route : BaseEntity
    {
        public string Name { get; set; }
        public IList<RouteBody> RouteBodies { get; set; }
        public IList<DocumentTemplate> DocumentTemplates { get; set; }
    }
}
