﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.Entities.Models.Interfaces
{
   public interface IEntity
   {
      int Id { get; set; }
      DateTime DateCreated { get; set; }
      DateTime? DateUpdated { get; set; }
   }
}
