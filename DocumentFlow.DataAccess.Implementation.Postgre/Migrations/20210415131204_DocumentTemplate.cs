﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DocumentFlow.DataAccess.Implementation.Postgre.Migrations
{
    public partial class DocumentTemplate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Documents_DocumentTemplate_DocumentTemplateId",
                table: "Documents");

            migrationBuilder.DropIndex(
                name: "IX_Documents_DocumentTemplateId",
                table: "Documents");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DocumentTemplate",
                table: "DocumentTemplate");

            migrationBuilder.RenameTable(
                name: "DocumentTemplate",
                newName: "DocumentTemplates");

            migrationBuilder.AddColumn<int>(
                name: "DocumentTemplateDocumentTypeId",
                table: "Documents",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DocumentTemplateRouteId",
                table: "Documents",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "DocumentTemplates",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer")
                .OldAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddColumn<int>(
                name: "RouteId",
                table: "DocumentTemplates",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DocumentTypeId",
                table: "DocumentTemplates",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_DocumentTemplates",
                table: "DocumentTemplates",
                columns: new[] { "RouteId", "DocumentTypeId" });

            migrationBuilder.InsertData(
                table: "DocumentTemplates",
                columns: new[] { "DocumentTypeId", "RouteId", "DateCreated", "DateUpdated", "Id" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(5703), null, 1 },
                    { 3, 2, new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(6457), null, 2 }
                });

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 808, DateTimeKind.Local).AddTicks(7844));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 809, DateTimeKind.Local).AddTicks(5564));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 809, DateTimeKind.Local).AddTicks(5578));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(8804));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9682));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9687));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9689));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9690));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9691));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9692));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9694));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9695));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9696));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9697));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9698));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9699));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9701));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9702));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(2731));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3897));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3901));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3903));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3904));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3905));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3907));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3908));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(2576));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3032));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3036));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3037));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3038));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3039));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3041));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3042));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3043));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(9223));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(9660));

            migrationBuilder.CreateIndex(
                name: "IX_Documents_DocumentTemplateRouteId_DocumentTemplateDocumentT~",
                table: "Documents",
                columns: new[] { "DocumentTemplateRouteId", "DocumentTemplateDocumentTypeId" });

            migrationBuilder.CreateIndex(
                name: "IX_DocumentTemplates_DocumentTypeId",
                table: "DocumentTemplates",
                column: "DocumentTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_DocumentTemplates_DocumentTemplateRouteId_Documen~",
                table: "Documents",
                columns: new[] { "DocumentTemplateRouteId", "DocumentTemplateDocumentTypeId" },
                principalTable: "DocumentTemplates",
                principalColumns: new[] { "RouteId", "DocumentTypeId" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentTemplates_DocumentTypes_DocumentTypeId",
                table: "DocumentTemplates",
                column: "DocumentTypeId",
                principalTable: "DocumentTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentTemplates_Routes_RouteId",
                table: "DocumentTemplates",
                column: "RouteId",
                principalTable: "Routes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Documents_DocumentTemplates_DocumentTemplateRouteId_Documen~",
                table: "Documents");

            migrationBuilder.DropForeignKey(
                name: "FK_DocumentTemplates_DocumentTypes_DocumentTypeId",
                table: "DocumentTemplates");

            migrationBuilder.DropForeignKey(
                name: "FK_DocumentTemplates_Routes_RouteId",
                table: "DocumentTemplates");

            migrationBuilder.DropIndex(
                name: "IX_Documents_DocumentTemplateRouteId_DocumentTemplateDocumentT~",
                table: "Documents");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DocumentTemplates",
                table: "DocumentTemplates");

            migrationBuilder.DropIndex(
                name: "IX_DocumentTemplates_DocumentTypeId",
                table: "DocumentTemplates");

            migrationBuilder.DeleteData(
                table: "DocumentTemplates",
                keyColumns: new[] { "DocumentTypeId", "RouteId" },
                keyColumnTypes: new[] { "integer", "integer" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "DocumentTemplates",
                keyColumns: new[] { "DocumentTypeId", "RouteId" },
                keyColumnTypes: new[] { "integer", "integer" },
                keyValues: new object[] { 3, 2 });

            migrationBuilder.DropColumn(
                name: "DocumentTemplateDocumentTypeId",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "DocumentTemplateRouteId",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "RouteId",
                table: "DocumentTemplates");

            migrationBuilder.DropColumn(
                name: "DocumentTypeId",
                table: "DocumentTemplates");

            migrationBuilder.RenameTable(
                name: "DocumentTemplates",
                newName: "DocumentTemplate");

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "DocumentTemplate",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer")
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_DocumentTemplate",
                table: "DocumentTemplate",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 861, DateTimeKind.Local).AddTicks(1359));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 862, DateTimeKind.Local).AddTicks(6967));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 862, DateTimeKind.Local).AddTicks(6988));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(2690));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3836));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3846));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3847));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3848));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3850));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3851));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3852));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3854));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3855));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3856));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3857));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3859));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3860));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3861));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 865, DateTimeKind.Local).AddTicks(9502));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 866, DateTimeKind.Local).AddTicks(1056));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 866, DateTimeKind.Local).AddTicks(1065));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 866, DateTimeKind.Local).AddTicks(1066));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 866, DateTimeKind.Local).AddTicks(1068));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 866, DateTimeKind.Local).AddTicks(1069));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 866, DateTimeKind.Local).AddTicks(1070));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 866, DateTimeKind.Local).AddTicks(1072));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(7457));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(8038));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(8046));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(8047));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(8048));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(8050));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(8051));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(8052));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(8053));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 865, DateTimeKind.Local).AddTicks(5399));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 865, DateTimeKind.Local).AddTicks(5971));

            migrationBuilder.CreateIndex(
                name: "IX_Documents_DocumentTemplateId",
                table: "Documents",
                column: "DocumentTemplateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_DocumentTemplate_DocumentTemplateId",
                table: "Documents",
                column: "DocumentTemplateId",
                principalTable: "DocumentTemplate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
