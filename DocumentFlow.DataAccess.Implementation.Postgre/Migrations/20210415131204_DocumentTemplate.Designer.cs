﻿// <auto-generated />
using System;
using DocumentFlow.DataAccess.Implementation.Postgre;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DocumentFlow.DataAccess.Implementation.Postgre.Migrations
{
    [DbContext(typeof(DataContext))]
    [Migration("20210415131204_DocumentTemplate")]
    partial class DocumentTemplate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 63)
                .HasAnnotation("ProductVersion", "5.0.4")
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            modelBuilder.Entity("DocumentFlow.Entities.Models.Attachment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<DateTime>("DateCreated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<DateTime?>("DateUpdated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int?>("DocumentId")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("DocumentId");

                    b.ToTable("Attachments");
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.Document", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<DateTime>("DateCreated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<DateTime?>("DateUpdated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int>("DocumentTemplateDocumentTypeId")
                        .HasColumnType("integer");

                    b.Property<int>("DocumentTemplateId")
                        .HasColumnType("integer");

                    b.Property<int>("DocumentTemplateRouteId")
                        .HasColumnType("integer");

                    b.Property<int?>("InitiatorId")
                        .HasColumnType("integer");

                    b.Property<string>("Name")
                        .HasColumnType("text");

                    b.Property<int>("State")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("InitiatorId");

                    b.HasIndex("DocumentTemplateRouteId", "DocumentTemplateDocumentTypeId");

                    b.ToTable("Documents");
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.DocumentAttachment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<int>("AttachmentId")
                        .HasColumnType("integer");

                    b.Property<DateTime>("DateCreated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<DateTime?>("DateUpdated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int>("DocumentId")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("AttachmentId");

                    b.HasIndex("DocumentId");

                    b.ToTable("DocumentAttachments");
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.DocumentState", b =>
                {
                    b.Property<int>("DocumentId")
                        .HasColumnType("integer");

                    b.Property<string>("Comment")
                        .HasColumnType("text");

                    b.Property<DateTime>("DateSend")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int>("EmployeeId")
                        .HasColumnType("integer");

                    b.Property<bool>("FlagInitiation")
                        .HasColumnType("boolean");

                    b.Property<int>("InitiatorDocumentStateId")
                        .HasColumnType("integer");

                    b.Property<int>("MyProperty")
                        .HasColumnType("integer");

                    b.Property<int>("PreviousDocumentStateId")
                        .HasColumnType("integer");

                    b.Property<int>("Result")
                        .HasColumnType("integer");

                    b.Property<int?>("RouteStageNextId")
                        .HasColumnType("integer");

                    b.HasKey("DocumentId");

                    b.HasIndex("EmployeeId");

                    b.HasIndex("InitiatorDocumentStateId")
                        .IsUnique();

                    b.HasIndex("PreviousDocumentStateId")
                        .IsUnique();

                    b.HasIndex("RouteStageNextId");

                    b.ToTable("DocumentStates");
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.DocumentStateTransaction", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Comment")
                        .HasColumnType("text");

                    b.Property<DateTime>("DateCreated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<DateTime>("DateSend")
                        .HasColumnType("timestamp without time zone");

                    b.Property<DateTime?>("DateUpdated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int>("DocumentId")
                        .HasColumnType("integer");

                    b.Property<int>("EmployeeId")
                        .HasColumnType("integer");

                    b.Property<bool>("FlagInitiation")
                        .HasColumnType("boolean");

                    b.Property<int>("InitiatorDocumentStateId")
                        .HasColumnType("integer");

                    b.Property<int>("MyProperty")
                        .HasColumnType("integer");

                    b.Property<int>("PreviousDocumentStateId")
                        .HasColumnType("integer");

                    b.Property<int>("Result")
                        .HasColumnType("integer");

                    b.Property<int?>("RouteStageNextId")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("DocumentId")
                        .IsUnique();

                    b.HasIndex("EmployeeId");

                    b.HasIndex("InitiatorDocumentStateId")
                        .IsUnique();

                    b.HasIndex("PreviousDocumentStateId")
                        .IsUnique();

                    b.HasIndex("RouteStageNextId");

                    b.ToTable("DocumentStateTransactions");
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.DocumentTemplate", b =>
                {
                    b.Property<int>("RouteId")
                        .HasColumnType("integer");

                    b.Property<int>("DocumentTypeId")
                        .HasColumnType("integer");

                    b.Property<DateTime>("DateCreated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<DateTime?>("DateUpdated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int>("Id")
                        .HasColumnType("integer");

                    b.HasKey("RouteId", "DocumentTypeId");

                    b.HasIndex("DocumentTypeId");

                    b.ToTable("DocumentTemplates");

                    b.HasData(
                        new
                        {
                            RouteId = 1,
                            DocumentTypeId = 1,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(5703),
                            Id = 1
                        },
                        new
                        {
                            RouteId = 2,
                            DocumentTypeId = 3,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(6457),
                            Id = 2
                        });
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.DocumentType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Code")
                        .HasColumnType("text");

                    b.Property<DateTime>("DateCreated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<DateTime?>("DateUpdated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("NameEn")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("DocumentTypes");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Code = "purord",
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 808, DateTimeKind.Local).AddTicks(7844),
                            NameEn = "Purchase Order"
                        },
                        new
                        {
                            Id = 2,
                            Code = "payord",
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 809, DateTimeKind.Local).AddTicks(5564),
                            NameEn = "Payment Order"
                        },
                        new
                        {
                            Id = 3,
                            Code = "joboffer",
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 809, DateTimeKind.Local).AddTicks(5578),
                            NameEn = "Job Offer"
                        });
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.Employee", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<DateTime>("DateCreated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<DateTime?>("DateUpdated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("FullName")
                        .HasColumnType("text");

                    b.Property<int>("JobTitle")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.ToTable("Employees");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(8804),
                            FullName = "Шилов Варлаам Андреевич",
                            JobTitle = 0
                        },
                        new
                        {
                            Id = 2,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9682),
                            FullName = "Аксёнов Авраам Робертович",
                            JobTitle = 0
                        },
                        new
                        {
                            Id = 3,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9687),
                            FullName = "Веселов Рудольф Степанович",
                            JobTitle = 1
                        },
                        new
                        {
                            Id = 4,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9689),
                            FullName = "Сидоров Семен Эдуардович",
                            JobTitle = 2
                        },
                        new
                        {
                            Id = 5,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9690),
                            FullName = "Копылов Ефим Наумович",
                            JobTitle = 3
                        },
                        new
                        {
                            Id = 6,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9691),
                            FullName = "Гурьев Агафон Феликсович",
                            JobTitle = 9
                        },
                        new
                        {
                            Id = 7,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9692),
                            FullName = "Баранов Назарий Куприянович",
                            JobTitle = 10
                        },
                        new
                        {
                            Id = 8,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9694),
                            FullName = "Кабанова Кристина Лаврентьевна",
                            JobTitle = 4
                        },
                        new
                        {
                            Id = 9,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9695),
                            FullName = "Журавлёва Земфира Андреевна",
                            JobTitle = 5
                        },
                        new
                        {
                            Id = 10,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9696),
                            FullName = "Александрова Ульяна Якуновна",
                            JobTitle = 6
                        },
                        new
                        {
                            Id = 11,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9697),
                            FullName = "Филатова Гелана Демьяновна",
                            JobTitle = 6
                        },
                        new
                        {
                            Id = 12,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9698),
                            FullName = "Корнилова Ирма Рудольфовна",
                            JobTitle = 6
                        },
                        new
                        {
                            Id = 13,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9699),
                            FullName = "Лаврентьева Амелия Игнатьевна",
                            JobTitle = 7
                        },
                        new
                        {
                            Id = 14,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9701),
                            FullName = "Филатова Ирина Денисовна",
                            JobTitle = 7
                        },
                        new
                        {
                            Id = 15,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9702),
                            FullName = "Богданова Людмила Авксентьевна",
                            JobTitle = 8
                        });
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.EmployeeRouteStage", b =>
                {
                    b.Property<int>("EmployeeId")
                        .HasColumnType("integer");

                    b.Property<int>("RouteStageId")
                        .HasColumnType("integer");

                    b.HasKey("EmployeeId", "RouteStageId");

                    b.HasIndex("RouteStageId");

                    b.ToTable("EmployeeRouteStages");

                    b.HasData(
                        new
                        {
                            EmployeeId = 1,
                            RouteStageId = -1
                        },
                        new
                        {
                            EmployeeId = 2,
                            RouteStageId = -1
                        },
                        new
                        {
                            EmployeeId = 3,
                            RouteStageId = -1
                        },
                        new
                        {
                            EmployeeId = 4,
                            RouteStageId = -1
                        },
                        new
                        {
                            EmployeeId = 5,
                            RouteStageId = 1
                        },
                        new
                        {
                            EmployeeId = 6,
                            RouteStageId = 6
                        },
                        new
                        {
                            EmployeeId = 7,
                            RouteStageId = 7
                        },
                        new
                        {
                            EmployeeId = 8,
                            RouteStageId = -1
                        },
                        new
                        {
                            EmployeeId = 8,
                            RouteStageId = 8
                        },
                        new
                        {
                            EmployeeId = 9,
                            RouteStageId = 2
                        },
                        new
                        {
                            EmployeeId = 10,
                            RouteStageId = 3
                        },
                        new
                        {
                            EmployeeId = 11,
                            RouteStageId = 3
                        },
                        new
                        {
                            EmployeeId = 12,
                            RouteStageId = 3
                        },
                        new
                        {
                            EmployeeId = 13,
                            RouteStageId = 4
                        },
                        new
                        {
                            EmployeeId = 14,
                            RouteStageId = 4
                        },
                        new
                        {
                            EmployeeId = 15,
                            RouteStageId = 5
                        });
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.Route", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<DateTime>("DateCreated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<DateTime?>("DateUpdated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("Name")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("Routes");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(9223),
                            Name = "Payment Order"
                        },
                        new
                        {
                            Id = 2,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(9660),
                            Name = "New Employee offer"
                        });
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.RouteBody", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<DateTime>("DateCreated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<DateTime?>("DateUpdated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int>("RouteId")
                        .HasColumnType("integer");

                    b.Property<int>("RouteStageId")
                        .HasColumnType("integer");

                    b.Property<int>("RouteStageOrder")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("RouteId");

                    b.HasIndex("RouteStageId");

                    b.ToTable("RouteBodies");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(2731),
                            RouteId = 1,
                            RouteStageId = 3,
                            RouteStageOrder = 1
                        },
                        new
                        {
                            Id = 2,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3897),
                            RouteId = 1,
                            RouteStageId = 4,
                            RouteStageOrder = 2
                        },
                        new
                        {
                            Id = 3,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3901),
                            RouteId = 1,
                            RouteStageId = 5,
                            RouteStageOrder = 3
                        },
                        new
                        {
                            Id = 4,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3903),
                            RouteId = 1,
                            RouteStageId = 6,
                            RouteStageOrder = 4
                        },
                        new
                        {
                            Id = 5,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3904),
                            RouteId = 1,
                            RouteStageId = 7,
                            RouteStageOrder = 5
                        },
                        new
                        {
                            Id = 6,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3905),
                            RouteId = 2,
                            RouteStageId = 8,
                            RouteStageOrder = 1
                        },
                        new
                        {
                            Id = 7,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3907),
                            RouteId = 2,
                            RouteStageId = 2,
                            RouteStageOrder = 2
                        },
                        new
                        {
                            Id = 8,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3908),
                            RouteId = 2,
                            RouteStageId = 6,
                            RouteStageOrder = 3
                        });
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.RouteStage", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<DateTime>("DateCreated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<DateTime?>("DateUpdated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("Description")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("RouteStages");

                    b.HasData(
                        new
                        {
                            Id = -1,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(2576),
                            Description = "Zero level"
                        },
                        new
                        {
                            Id = 1,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3032),
                            Description = "Chief of development department"
                        },
                        new
                        {
                            Id = 2,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3036),
                            Description = "Chief of HR"
                        },
                        new
                        {
                            Id = 3,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3037),
                            Description = "Accountants"
                        },
                        new
                        {
                            Id = 4,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3038),
                            Description = "Seniors accountants"
                        },
                        new
                        {
                            Id = 5,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3039),
                            Description = "Chief of accountant"
                        },
                        new
                        {
                            Id = 6,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3041),
                            Description = "Financial Director"
                        },
                        new
                        {
                            Id = 7,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3042),
                            Description = "General Director"
                        },
                        new
                        {
                            Id = 8,
                            DateCreated = new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3043),
                            Description = "HR Manager"
                        });
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.Attachment", b =>
                {
                    b.HasOne("DocumentFlow.Entities.Models.Document", null)
                        .WithMany("Attachment")
                        .HasForeignKey("DocumentId");
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.Document", b =>
                {
                    b.HasOne("DocumentFlow.Entities.Models.Employee", "Initiator")
                        .WithMany()
                        .HasForeignKey("InitiatorId");

                    b.HasOne("DocumentFlow.Entities.Models.DocumentTemplate", "DocumentTemplate")
                        .WithMany()
                        .HasForeignKey("DocumentTemplateRouteId", "DocumentTemplateDocumentTypeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("DocumentTemplate");

                    b.Navigation("Initiator");
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.DocumentAttachment", b =>
                {
                    b.HasOne("DocumentFlow.Entities.Models.Attachment", "Attachment")
                        .WithMany()
                        .HasForeignKey("AttachmentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DocumentFlow.Entities.Models.Document", "Document")
                        .WithMany()
                        .HasForeignKey("DocumentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Attachment");

                    b.Navigation("Document");
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.DocumentState", b =>
                {
                    b.HasOne("DocumentFlow.Entities.Models.Document", "Document")
                        .WithOne()
                        .HasForeignKey("DocumentFlow.Entities.Models.DocumentState", "DocumentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DocumentFlow.Entities.Models.Employee", "Employee")
                        .WithMany()
                        .HasForeignKey("EmployeeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DocumentFlow.Entities.Models.DocumentStateTransaction", "InitiatorDocumentState")
                        .WithOne()
                        .HasForeignKey("DocumentFlow.Entities.Models.DocumentState", "InitiatorDocumentStateId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DocumentFlow.Entities.Models.DocumentStateTransaction", "PreviousDocumentState")
                        .WithOne()
                        .HasForeignKey("DocumentFlow.Entities.Models.DocumentState", "PreviousDocumentStateId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DocumentFlow.Entities.Models.RouteStage", "RouteStageNext")
                        .WithMany()
                        .HasForeignKey("RouteStageNextId");

                    b.Navigation("Document");

                    b.Navigation("Employee");

                    b.Navigation("InitiatorDocumentState");

                    b.Navigation("PreviousDocumentState");

                    b.Navigation("RouteStageNext");
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.DocumentStateTransaction", b =>
                {
                    b.HasOne("DocumentFlow.Entities.Models.Document", "Document")
                        .WithOne()
                        .HasForeignKey("DocumentFlow.Entities.Models.DocumentStateTransaction", "DocumentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DocumentFlow.Entities.Models.Employee", "Employee")
                        .WithMany()
                        .HasForeignKey("EmployeeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DocumentFlow.Entities.Models.DocumentStateTransaction", "InitiatorDocumentState")
                        .WithOne()
                        .HasForeignKey("DocumentFlow.Entities.Models.DocumentStateTransaction", "InitiatorDocumentStateId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DocumentFlow.Entities.Models.DocumentStateTransaction", "PreviousDocumentState")
                        .WithOne()
                        .HasForeignKey("DocumentFlow.Entities.Models.DocumentStateTransaction", "PreviousDocumentStateId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DocumentFlow.Entities.Models.RouteStage", "RouteStageNext")
                        .WithMany()
                        .HasForeignKey("RouteStageNextId");

                    b.Navigation("Document");

                    b.Navigation("Employee");

                    b.Navigation("InitiatorDocumentState");

                    b.Navigation("PreviousDocumentState");

                    b.Navigation("RouteStageNext");
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.DocumentTemplate", b =>
                {
                    b.HasOne("DocumentFlow.Entities.Models.DocumentType", "DocumentType")
                        .WithMany("DocumentTemplates")
                        .HasForeignKey("DocumentTypeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DocumentFlow.Entities.Models.Route", "Route")
                        .WithMany("DocumentTemplates")
                        .HasForeignKey("RouteId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("DocumentType");

                    b.Navigation("Route");
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.EmployeeRouteStage", b =>
                {
                    b.HasOne("DocumentFlow.Entities.Models.Employee", "Employee")
                        .WithMany("EmployeeRouteStages")
                        .HasForeignKey("EmployeeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DocumentFlow.Entities.Models.RouteStage", "RouteStage")
                        .WithMany("EmployeeRouteStages")
                        .HasForeignKey("RouteStageId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Employee");

                    b.Navigation("RouteStage");
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.RouteBody", b =>
                {
                    b.HasOne("DocumentFlow.Entities.Models.Route", "Route")
                        .WithMany("RouteBodies")
                        .HasForeignKey("RouteId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DocumentFlow.Entities.Models.RouteStage", "RouteStage")
                        .WithMany()
                        .HasForeignKey("RouteStageId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Route");

                    b.Navigation("RouteStage");
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.Document", b =>
                {
                    b.Navigation("Attachment");
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.DocumentType", b =>
                {
                    b.Navigation("DocumentTemplates");
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.Employee", b =>
                {
                    b.Navigation("EmployeeRouteStages");
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.Route", b =>
                {
                    b.Navigation("DocumentTemplates");

                    b.Navigation("RouteBodies");
                });

            modelBuilder.Entity("DocumentFlow.Entities.Models.RouteStage", b =>
                {
                    b.Navigation("EmployeeRouteStages");
                });
#pragma warning restore 612, 618
        }
    }
}
