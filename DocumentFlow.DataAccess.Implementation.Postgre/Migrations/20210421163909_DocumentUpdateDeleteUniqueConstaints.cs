﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DocumentFlow.DataAccess.Implementation.Postgre.Migrations
{
    public partial class DocumentUpdateDeleteUniqueConstaints : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_DocumentStateTransactions_DocumentId",
                table: "DocumentStateTransactions");

            migrationBuilder.DropIndex(
                name: "IX_DocumentStateTransactions_InitiatorDocumentStateId",
                table: "DocumentStateTransactions");

            migrationBuilder.DropIndex(
                name: "IX_DocumentStateTransactions_PreviousDocumentStateId",
                table: "DocumentStateTransactions");

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(9727));

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 836, DateTimeKind.Local).AddTicks(656));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 831, DateTimeKind.Local).AddTicks(4421));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 832, DateTimeKind.Local).AddTicks(5841));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 832, DateTimeKind.Local).AddTicks(5897));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(689));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1708));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1716));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1718));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1719));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1720));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1721));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1722));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1724));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1725));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1726));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1727));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1729));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1730));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1731));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(6018));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(7455));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(7462));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(7464));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(7465));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(7466));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(7506));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(7509));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(4942));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(5462));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(5469));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(5470));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(5471));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(5473));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(5474));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(5475));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(5476));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(2275));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(2778));

            migrationBuilder.CreateIndex(
                name: "IX_DocumentStateTransactions_DocumentId",
                table: "DocumentStateTransactions",
                column: "DocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentStateTransactions_InitiatorDocumentStateId",
                table: "DocumentStateTransactions",
                column: "InitiatorDocumentStateId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentStateTransactions_PreviousDocumentStateId",
                table: "DocumentStateTransactions",
                column: "PreviousDocumentStateId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_DocumentStateTransactions_DocumentId",
                table: "DocumentStateTransactions");

            migrationBuilder.DropIndex(
                name: "IX_DocumentStateTransactions_InitiatorDocumentStateId",
                table: "DocumentStateTransactions");

            migrationBuilder.DropIndex(
                name: "IX_DocumentStateTransactions_PreviousDocumentStateId",
                table: "DocumentStateTransactions");

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 77, DateTimeKind.Local).AddTicks(968));

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 77, DateTimeKind.Local).AddTicks(1921));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 72, DateTimeKind.Local).AddTicks(3483));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 73, DateTimeKind.Local).AddTicks(5580));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 73, DateTimeKind.Local).AddTicks(5601));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(1397));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2510));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2518));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2519));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2521));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2522));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2523));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2525));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2526));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2528));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2529));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2530));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2532));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2533));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2534));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 76, DateTimeKind.Local).AddTicks(7137));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 76, DateTimeKind.Local).AddTicks(8574));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 76, DateTimeKind.Local).AddTicks(8582));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 76, DateTimeKind.Local).AddTicks(8584));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 76, DateTimeKind.Local).AddTicks(8585));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 76, DateTimeKind.Local).AddTicks(8586));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 76, DateTimeKind.Local).AddTicks(8588));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 76, DateTimeKind.Local).AddTicks(8589));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(5832));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(6361));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(6368));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(6370));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(6371));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(6373));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(6374));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(6375));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(6376));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 76, DateTimeKind.Local).AddTicks(3270));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 76, DateTimeKind.Local).AddTicks(3789));

            migrationBuilder.CreateIndex(
                name: "IX_DocumentStateTransactions_DocumentId",
                table: "DocumentStateTransactions",
                column: "DocumentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DocumentStateTransactions_InitiatorDocumentStateId",
                table: "DocumentStateTransactions",
                column: "InitiatorDocumentStateId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DocumentStateTransactions_PreviousDocumentStateId",
                table: "DocumentStateTransactions",
                column: "PreviousDocumentStateId",
                unique: true);
        }
    }
}
