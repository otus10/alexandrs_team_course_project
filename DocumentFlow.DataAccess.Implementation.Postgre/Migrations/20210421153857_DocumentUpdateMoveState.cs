﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DocumentFlow.DataAccess.Implementation.Postgre.Migrations
{
    public partial class DocumentUpdateMoveState : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocumentStates_RouteStages_RouteStageNextId",
                table: "DocumentStates");

            migrationBuilder.DropForeignKey(
                name: "FK_DocumentStateTransactions_RouteStages_RouteStageNextId",
                table: "DocumentStateTransactions");

            migrationBuilder.DropColumn(
                name: "State",
                table: "Documents");

            migrationBuilder.AlterColumn<int>(
                name: "RouteStageNextId",
                table: "DocumentStateTransactions",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "State",
                table: "DocumentStateTransactions",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "RouteStageNextId",
                table: "DocumentStates",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CurrentDocumentStateId",
                table: "DocumentStates",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "State",
                table: "DocumentStates",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 77, DateTimeKind.Local).AddTicks(968));

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 77, DateTimeKind.Local).AddTicks(1921));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 72, DateTimeKind.Local).AddTicks(3483));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 73, DateTimeKind.Local).AddTicks(5580));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 73, DateTimeKind.Local).AddTicks(5601));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(1397));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2510));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2518));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2519));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2521));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2522));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2523));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2525));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2526));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2528));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2529));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2530));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2532));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2533));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(2534));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 76, DateTimeKind.Local).AddTicks(7137));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 76, DateTimeKind.Local).AddTicks(8574));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 76, DateTimeKind.Local).AddTicks(8582));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 76, DateTimeKind.Local).AddTicks(8584));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 76, DateTimeKind.Local).AddTicks(8585));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 76, DateTimeKind.Local).AddTicks(8586));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 76, DateTimeKind.Local).AddTicks(8588));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 76, DateTimeKind.Local).AddTicks(8589));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(5832));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(6361));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(6368));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(6370));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(6371));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(6373));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(6374));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(6375));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 75, DateTimeKind.Local).AddTicks(6376));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 76, DateTimeKind.Local).AddTicks(3270));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 18, 38, 57, 76, DateTimeKind.Local).AddTicks(3789));

            migrationBuilder.CreateIndex(
                name: "IX_DocumentStates_CurrentDocumentStateId",
                table: "DocumentStates",
                column: "CurrentDocumentStateId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentStates_DocumentStateTransactions_CurrentDocumentSta~",
                table: "DocumentStates",
                column: "CurrentDocumentStateId",
                principalTable: "DocumentStateTransactions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentStates_RouteStages_RouteStageNextId",
                table: "DocumentStates",
                column: "RouteStageNextId",
                principalTable: "RouteStages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentStateTransactions_RouteStages_RouteStageNextId",
                table: "DocumentStateTransactions",
                column: "RouteStageNextId",
                principalTable: "RouteStages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocumentStates_DocumentStateTransactions_CurrentDocumentSta~",
                table: "DocumentStates");

            migrationBuilder.DropForeignKey(
                name: "FK_DocumentStates_RouteStages_RouteStageNextId",
                table: "DocumentStates");

            migrationBuilder.DropForeignKey(
                name: "FK_DocumentStateTransactions_RouteStages_RouteStageNextId",
                table: "DocumentStateTransactions");

            migrationBuilder.DropIndex(
                name: "IX_DocumentStates_CurrentDocumentStateId",
                table: "DocumentStates");

            migrationBuilder.DropColumn(
                name: "State",
                table: "DocumentStateTransactions");

            migrationBuilder.DropColumn(
                name: "CurrentDocumentStateId",
                table: "DocumentStates");

            migrationBuilder.DropColumn(
                name: "State",
                table: "DocumentStates");

            migrationBuilder.AlterColumn<int>(
                name: "RouteStageNextId",
                table: "DocumentStateTransactions",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "RouteStageNextId",
                table: "DocumentStates",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddColumn<int>(
                name: "State",
                table: "Documents",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 751, DateTimeKind.Local).AddTicks(9950));

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 752, DateTimeKind.Local).AddTicks(1632));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 738, DateTimeKind.Local).AddTicks(9841));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 741, DateTimeKind.Local).AddTicks(6869));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 741, DateTimeKind.Local).AddTicks(6929));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 745, DateTimeKind.Local).AddTicks(8161));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 746, DateTimeKind.Local).AddTicks(613));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 746, DateTimeKind.Local).AddTicks(628));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 746, DateTimeKind.Local).AddTicks(631));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 746, DateTimeKind.Local).AddTicks(635));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 746, DateTimeKind.Local).AddTicks(638));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 746, DateTimeKind.Local).AddTicks(642));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 746, DateTimeKind.Local).AddTicks(645));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 746, DateTimeKind.Local).AddTicks(648));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 746, DateTimeKind.Local).AddTicks(651));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 746, DateTimeKind.Local).AddTicks(655));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 746, DateTimeKind.Local).AddTicks(658));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 746, DateTimeKind.Local).AddTicks(662));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 746, DateTimeKind.Local).AddTicks(665));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 746, DateTimeKind.Local).AddTicks(668));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 751, DateTimeKind.Local).AddTicks(2411));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 751, DateTimeKind.Local).AddTicks(4843));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 751, DateTimeKind.Local).AddTicks(4855));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 751, DateTimeKind.Local).AddTicks(4857));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 751, DateTimeKind.Local).AddTicks(4860));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 751, DateTimeKind.Local).AddTicks(4862));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 751, DateTimeKind.Local).AddTicks(4865));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 751, DateTimeKind.Local).AddTicks(4867));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 747, DateTimeKind.Local).AddTicks(126));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 747, DateTimeKind.Local).AddTicks(1398));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 747, DateTimeKind.Local).AddTicks(1409));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 747, DateTimeKind.Local).AddTicks(1413));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 747, DateTimeKind.Local).AddTicks(1416));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 747, DateTimeKind.Local).AddTicks(1418));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 747, DateTimeKind.Local).AddTicks(1421));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 747, DateTimeKind.Local).AddTicks(1425));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 747, DateTimeKind.Local).AddTicks(1428));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 750, DateTimeKind.Local).AddTicks(2906));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 19, 22, 41, 36, 750, DateTimeKind.Local).AddTicks(4424));

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentStates_RouteStages_RouteStageNextId",
                table: "DocumentStates",
                column: "RouteStageNextId",
                principalTable: "RouteStages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentStateTransactions_RouteStages_RouteStageNextId",
                table: "DocumentStateTransactions",
                column: "RouteStageNextId",
                principalTable: "RouteStages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
