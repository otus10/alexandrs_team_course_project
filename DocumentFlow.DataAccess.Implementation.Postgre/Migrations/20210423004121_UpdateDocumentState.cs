﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DocumentFlow.DataAccess.Implementation.Postgre.Migrations
{
    public partial class UpdateDocumentState : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocumentStates_DocumentStateTransactions_PreviousDocumentSt~",
                table: "DocumentStates");

            migrationBuilder.DropForeignKey(
                name: "FK_DocumentStateTransactions_DocumentStateTransactions_Initiat~",
                table: "DocumentStateTransactions");

            migrationBuilder.DropForeignKey(
                name: "FK_DocumentStateTransactions_DocumentStateTransactions_Previou~",
                table: "DocumentStateTransactions");

            migrationBuilder.AddColumn<int>(
                name: "RouteStageOrder",
                table: "DocumentStateTransactions",
                type: "integer",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "InitiatorDocumentStateId",
                table: "DocumentStates",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RouteStageOrder",
                table: "DocumentStates",
                type: "integer",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 763, DateTimeKind.Local).AddTicks(6849));

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 763, DateTimeKind.Local).AddTicks(8341));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 754, DateTimeKind.Local).AddTicks(8891));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 757, DateTimeKind.Local).AddTicks(4261));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 757, DateTimeKind.Local).AddTicks(4312));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(1823));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3691));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3701));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3704));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3706));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3709));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3711));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3713));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3715));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3718));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3822));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3825));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3828));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3830));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3832));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 763, DateTimeKind.Local).AddTicks(264));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 763, DateTimeKind.Local).AddTicks(2640));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 763, DateTimeKind.Local).AddTicks(2650));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 763, DateTimeKind.Local).AddTicks(2653));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 763, DateTimeKind.Local).AddTicks(2656));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 763, DateTimeKind.Local).AddTicks(2658));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 763, DateTimeKind.Local).AddTicks(2660));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 763, DateTimeKind.Local).AddTicks(2662));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(9651));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 761, DateTimeKind.Local).AddTicks(530));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 761, DateTimeKind.Local).AddTicks(538));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 761, DateTimeKind.Local).AddTicks(541));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 761, DateTimeKind.Local).AddTicks(543));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 761, DateTimeKind.Local).AddTicks(545));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 761, DateTimeKind.Local).AddTicks(547));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 761, DateTimeKind.Local).AddTicks(550));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 761, DateTimeKind.Local).AddTicks(552));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 762, DateTimeKind.Local).AddTicks(3547));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 762, DateTimeKind.Local).AddTicks(4444));

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentStates_DocumentStateTransactions_PreviousDocumentSt~",
                table: "DocumentStates",
                column: "PreviousDocumentStateId",
                principalTable: "DocumentStateTransactions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentStateTransactions_DocumentStateTransactions_Initiat~",
                table: "DocumentStateTransactions",
                column: "InitiatorDocumentStateId",
                principalTable: "DocumentStateTransactions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentStateTransactions_DocumentStateTransactions_Previou~",
                table: "DocumentStateTransactions",
                column: "PreviousDocumentStateId",
                principalTable: "DocumentStateTransactions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocumentStates_DocumentStateTransactions_PreviousDocumentSt~",
                table: "DocumentStates");

            migrationBuilder.DropForeignKey(
                name: "FK_DocumentStateTransactions_DocumentStateTransactions_Initiat~",
                table: "DocumentStateTransactions");

            migrationBuilder.DropForeignKey(
                name: "FK_DocumentStateTransactions_DocumentStateTransactions_Previou~",
                table: "DocumentStateTransactions");

            migrationBuilder.DropColumn(
                name: "RouteStageOrder",
                table: "DocumentStateTransactions");

            migrationBuilder.DropColumn(
                name: "RouteStageOrder",
                table: "DocumentStates");

            migrationBuilder.AlterColumn<int>(
                name: "InitiatorDocumentStateId",
                table: "DocumentStates",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(9727));

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 836, DateTimeKind.Local).AddTicks(656));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 831, DateTimeKind.Local).AddTicks(4421));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 832, DateTimeKind.Local).AddTicks(5841));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 832, DateTimeKind.Local).AddTicks(5897));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(689));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1708));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1716));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1718));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1719));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1720));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1721));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1722));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1724));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1725));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1726));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1727));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1729));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1730));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(1731));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(6018));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(7455));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(7462));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(7464));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(7465));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(7466));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(7506));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(7509));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(4942));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(5462));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(5469));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(5470));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(5471));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(5473));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(5474));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(5475));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 834, DateTimeKind.Local).AddTicks(5476));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(2275));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 21, 19, 39, 8, 835, DateTimeKind.Local).AddTicks(2778));

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentStates_DocumentStateTransactions_PreviousDocumentSt~",
                table: "DocumentStates",
                column: "PreviousDocumentStateId",
                principalTable: "DocumentStateTransactions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentStateTransactions_DocumentStateTransactions_Initiat~",
                table: "DocumentStateTransactions",
                column: "InitiatorDocumentStateId",
                principalTable: "DocumentStateTransactions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentStateTransactions_DocumentStateTransactions_Previou~",
                table: "DocumentStateTransactions",
                column: "PreviousDocumentStateId",
                principalTable: "DocumentStateTransactions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
