﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DocumentFlow.DataAccess.Implementation.Postgre.Migrations
{
    public partial class AttachmentUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Documents_DocumentTemplates_DocumentTemplateRouteId_Documen~",
                table: "Documents");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DocumentTemplates",
                table: "DocumentTemplates");

            migrationBuilder.DropIndex(
                name: "IX_Documents_DocumentTemplateRouteId_DocumentTemplateDocumentT~",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "DocumentTemplateDocumentTypeId",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "DocumentTemplateRouteId",
                table: "Documents");

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "DocumentTemplates",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer")
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddColumn<string>(
                name: "FileName",
                table: "Attachments",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FilePath",
                table: "Attachments",
                type: "text",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_DocumentTemplates",
                table: "DocumentTemplates",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 9, DateTimeKind.Local).AddTicks(6901));

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 9, DateTimeKind.Local).AddTicks(7825));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 5, DateTimeKind.Local).AddTicks(863));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 6, DateTimeKind.Local).AddTicks(2367));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 6, DateTimeKind.Local).AddTicks(2386));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 7, DateTimeKind.Local).AddTicks(7658));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 7, DateTimeKind.Local).AddTicks(8687));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 7, DateTimeKind.Local).AddTicks(8695));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 7, DateTimeKind.Local).AddTicks(8696));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 7, DateTimeKind.Local).AddTicks(8698));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 7, DateTimeKind.Local).AddTicks(8699));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 7, DateTimeKind.Local).AddTicks(8701));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 7, DateTimeKind.Local).AddTicks(8702));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 7, DateTimeKind.Local).AddTicks(8704));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 7, DateTimeKind.Local).AddTicks(8705));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 7, DateTimeKind.Local).AddTicks(8706));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 7, DateTimeKind.Local).AddTicks(8707));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 7, DateTimeKind.Local).AddTicks(8709));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 7, DateTimeKind.Local).AddTicks(8710));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 7, DateTimeKind.Local).AddTicks(8711));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 9, DateTimeKind.Local).AddTicks(3357));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 9, DateTimeKind.Local).AddTicks(4751));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 9, DateTimeKind.Local).AddTicks(4759));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 9, DateTimeKind.Local).AddTicks(4761));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 9, DateTimeKind.Local).AddTicks(4762));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 9, DateTimeKind.Local).AddTicks(4763));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 9, DateTimeKind.Local).AddTicks(4765));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 9, DateTimeKind.Local).AddTicks(4766));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 8, DateTimeKind.Local).AddTicks(1845));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 8, DateTimeKind.Local).AddTicks(2368));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 8, DateTimeKind.Local).AddTicks(2375));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 8, DateTimeKind.Local).AddTicks(2376));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 8, DateTimeKind.Local).AddTicks(2378));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 8, DateTimeKind.Local).AddTicks(2379));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 8, DateTimeKind.Local).AddTicks(2380));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 8, DateTimeKind.Local).AddTicks(2382));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 8, DateTimeKind.Local).AddTicks(2383));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 8, DateTimeKind.Local).AddTicks(9443));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 17, 23, 15, 16, 8, DateTimeKind.Local).AddTicks(9960));

            migrationBuilder.CreateIndex(
                name: "IX_DocumentTemplates_RouteId",
                table: "DocumentTemplates",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_DocumentTemplateId",
                table: "Documents",
                column: "DocumentTemplateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_DocumentTemplates_DocumentTemplateId",
                table: "Documents",
                column: "DocumentTemplateId",
                principalTable: "DocumentTemplates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Documents_DocumentTemplates_DocumentTemplateId",
                table: "Documents");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DocumentTemplates",
                table: "DocumentTemplates");

            migrationBuilder.DropIndex(
                name: "IX_DocumentTemplates_RouteId",
                table: "DocumentTemplates");

            migrationBuilder.DropIndex(
                name: "IX_Documents_DocumentTemplateId",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "FileName",
                table: "Attachments");

            migrationBuilder.DropColumn(
                name: "FilePath",
                table: "Attachments");

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "DocumentTemplates",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer")
                .OldAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddColumn<int>(
                name: "DocumentTemplateDocumentTypeId",
                table: "Documents",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DocumentTemplateRouteId",
                table: "Documents",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_DocumentTemplates",
                table: "DocumentTemplates",
                columns: new[] { "RouteId", "DocumentTypeId" });

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumns: new[] { "DocumentTypeId", "RouteId" },
                keyValues: new object[] { 1, 1 },
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(6085));

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumns: new[] { "DocumentTypeId", "RouteId" },
                keyValues: new object[] { 3, 2 },
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(6846));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 200, DateTimeKind.Local).AddTicks(8173));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 201, DateTimeKind.Local).AddTicks(6218));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 201, DateTimeKind.Local).AddTicks(6231));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 202, DateTimeKind.Local).AddTicks(9377));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(248));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(293));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(295));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(296));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(298));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(299));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(300));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(301));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(302));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(304));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(305));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(306));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(307));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(308));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(3133));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(4281));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(4287));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(4288));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(4289));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(4291));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(4292));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(4293));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(3193));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(3650));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(3654));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(3656));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(3657));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(3658));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(3659));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(3660));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(3661));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(9750));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(182));

            migrationBuilder.CreateIndex(
                name: "IX_Documents_DocumentTemplateRouteId_DocumentTemplateDocumentT~",
                table: "Documents",
                columns: new[] { "DocumentTemplateRouteId", "DocumentTemplateDocumentTypeId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_DocumentTemplates_DocumentTemplateRouteId_Documen~",
                table: "Documents",
                columns: new[] { "DocumentTemplateRouteId", "DocumentTemplateDocumentTypeId" },
                principalTable: "DocumentTemplates",
                principalColumns: new[] { "RouteId", "DocumentTypeId" },
                onDelete: ReferentialAction.Cascade);
        }
    }
}
