﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DocumentFlow.DataAccess.Implementation.Postgre.Migrations
{
    public partial class RouteStageNextIdNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocumentStates_RouteStages_RouteStageNextId",
                table: "DocumentStates");

            migrationBuilder.DropForeignKey(
                name: "FK_DocumentStateTransactions_RouteStages_RouteStageNextId",
                table: "DocumentStateTransactions");

            migrationBuilder.AlterColumn<int>(
                name: "RouteStageNextId",
                table: "DocumentStateTransactions",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "RouteStageNextId",
                table: "DocumentStates",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 204, DateTimeKind.Local).AddTicks(450));

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 204, DateTimeKind.Local).AddTicks(1040));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 200, DateTimeKind.Local).AddTicks(9567));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 201, DateTimeKind.Local).AddTicks(6106));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 201, DateTimeKind.Local).AddTicks(6119));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(6727));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7402));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7408));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7409));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7410));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7411));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7412));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7413));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7414));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7415));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7416));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7417));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7418));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7419));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7420));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(7917));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(8783));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(8788));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(8789));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(8790));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(8791));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(8793));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(8794));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(9824));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(175));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(179));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(180));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(181));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(182));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(183));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(184));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(185));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(5176));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(5509));

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentStates_RouteStages_RouteStageNextId",
                table: "DocumentStates",
                column: "RouteStageNextId",
                principalTable: "RouteStages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentStateTransactions_RouteStages_RouteStageNextId",
                table: "DocumentStateTransactions",
                column: "RouteStageNextId",
                principalTable: "RouteStages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocumentStates_RouteStages_RouteStageNextId",
                table: "DocumentStates");

            migrationBuilder.DropForeignKey(
                name: "FK_DocumentStateTransactions_RouteStages_RouteStageNextId",
                table: "DocumentStateTransactions");

            migrationBuilder.AlterColumn<int>(
                name: "RouteStageNextId",
                table: "DocumentStateTransactions",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RouteStageNextId",
                table: "DocumentStates",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 763, DateTimeKind.Local).AddTicks(6849));

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 763, DateTimeKind.Local).AddTicks(8341));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 754, DateTimeKind.Local).AddTicks(8891));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 757, DateTimeKind.Local).AddTicks(4261));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 757, DateTimeKind.Local).AddTicks(4312));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(1823));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3691));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3701));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3704));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3706));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3709));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3711));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3713));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3715));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3718));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3822));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3825));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3828));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3830));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(3832));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 763, DateTimeKind.Local).AddTicks(264));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 763, DateTimeKind.Local).AddTicks(2640));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 763, DateTimeKind.Local).AddTicks(2650));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 763, DateTimeKind.Local).AddTicks(2653));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 763, DateTimeKind.Local).AddTicks(2656));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 763, DateTimeKind.Local).AddTicks(2658));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 763, DateTimeKind.Local).AddTicks(2660));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 763, DateTimeKind.Local).AddTicks(2662));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 760, DateTimeKind.Local).AddTicks(9651));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 761, DateTimeKind.Local).AddTicks(530));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 761, DateTimeKind.Local).AddTicks(538));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 761, DateTimeKind.Local).AddTicks(541));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 761, DateTimeKind.Local).AddTicks(543));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 761, DateTimeKind.Local).AddTicks(545));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 761, DateTimeKind.Local).AddTicks(547));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 761, DateTimeKind.Local).AddTicks(550));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 761, DateTimeKind.Local).AddTicks(552));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 762, DateTimeKind.Local).AddTicks(3547));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 23, 3, 41, 19, 762, DateTimeKind.Local).AddTicks(4444));

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentStates_RouteStages_RouteStageNextId",
                table: "DocumentStates",
                column: "RouteStageNextId",
                principalTable: "RouteStages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentStateTransactions_RouteStages_RouteStageNextId",
                table: "DocumentStateTransactions",
                column: "RouteStageNextId",
                principalTable: "RouteStages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
