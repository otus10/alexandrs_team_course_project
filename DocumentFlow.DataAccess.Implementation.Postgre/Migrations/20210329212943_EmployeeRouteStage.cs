﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DocumentFlow.DataAccess.Implementation.Postgre.Migrations
{
    public partial class EmployeeRouteStage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FullName = table.Column<string>(type: "text", nullable: true),
                    JobTitle = table.Column<int>(type: "integer", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RouteStages",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Description = table.Column<string>(type: "text", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RouteStages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeRouteStages",
                columns: table => new
                {
                    EmployeeId = table.Column<int>(type: "integer", nullable: false),
                    RouteStageId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeRouteStages", x => new { x.EmployeeId, x.RouteStageId });
                    table.ForeignKey(
                        name: "FK_EmployeeRouteStages_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmployeeRouteStages_RouteStages_RouteStageId",
                        column: x => x.RouteStageId,
                        principalTable: "RouteStages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 3, 30, 0, 29, 40, 735, DateTimeKind.Local).AddTicks(2100));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 3, 30, 0, 29, 40, 738, DateTimeKind.Local).AddTicks(1188));

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "DateCreated", "DateUpdated", "FullName", "JobTitle" },
                values: new object[,]
                {
                    { 15, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(1559), null, "Богданова Людмила Авксентьевна", 8 },
                    { 14, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(1556), null, "Филатова Ирина Денисовна", 7 },
                    { 12, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(1552), null, "Корнилова Ирма Рудольфовна", 6 },
                    { 11, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(1550), null, "Филатова Гелана Демьяновна", 6 },
                    { 10, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(1547), null, "Александрова Ульяна Якуновна", 6 },
                    { 9, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(1545), null, "Журавлёва Земфира Андреевна", 5 },
                    { 13, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(1554), null, "Лаврентьева Амелия Игнатьевна", 7 },
                    { 7, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(1541), null, "Баранов Назарий Куприянович", 10 },
                    { 8, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(1543), null, "Кабанова Кристина Лаврентьевна", 4 },
                    { 2, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(1522), null, "Аксёнов Авраам Робертович", 0 },
                    { 3, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(1531), null, "Веселов Рудольф Степанович", 1 },
                    { 1, new DateTime(2021, 3, 30, 0, 29, 40, 798, DateTimeKind.Local).AddTicks(9767), null, "Шилов Варлаам Андреевич", 0 },
                    { 5, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(1536), null, "Копылов Ефим Наумович", 3 },
                    { 6, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(1538), null, "Гурьев Агафон Феликсович", 9 },
                    { 4, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(1534), null, "Сидоров Семен Эдуардович", 2 }
                });

            migrationBuilder.InsertData(
                table: "RouteStages",
                columns: new[] { "Id", "DateCreated", "DateUpdated", "Description" },
                values: new object[,]
                {
                    { -1, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(7989), null, "Zero level" },
                    { 1, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(8926), null, "Chief of development department" },
                    { 3, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(8938), null, "Accountants" },
                    { 4, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(8940), null, "Seniors accountants" },
                    { 5, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(8942), null, "Chief of accountant" },
                    { 6, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(8945), null, "Financial Director" },
                    { 7, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(8947), null, "General Director" },
                    { 8, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(8949), null, "HR Manager" },
                    { 2, new DateTime(2021, 3, 30, 0, 29, 40, 799, DateTimeKind.Local).AddTicks(8935), null, "Chief of HR" }
                });

            migrationBuilder.InsertData(
                table: "EmployeeRouteStages",
                columns: new[] { "EmployeeId", "RouteStageId" },
                values: new object[,]
                {
                    { 1, -1 },
                    { 2, -1 },
                    { 3, -1 },
                    { 4, -1 },
                    { 8, -1 },
                    { 5, 1 },
                    { 9, 2 },
                    { 10, 3 },
                    { 11, 3 },
                    { 12, 3 },
                    { 13, 4 },
                    { 14, 4 },
                    { 15, 5 },
                    { 6, 6 },
                    { 7, 7 },
                    { 8, 8 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeRouteStages_RouteStageId",
                table: "EmployeeRouteStages",
                column: "RouteStageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmployeeRouteStages");

            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "RouteStages");

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 3, 17, 22, 55, 46, 622, DateTimeKind.Local).AddTicks(8086));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 3, 17, 22, 55, 46, 624, DateTimeKind.Local).AddTicks(814));
        }
    }
}
