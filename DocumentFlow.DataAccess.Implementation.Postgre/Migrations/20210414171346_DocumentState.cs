﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DocumentFlow.DataAccess.Implementation.Postgre.Migrations
{
    public partial class DocumentState : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DocumentStateTransactions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DocumentId = table.Column<int>(type: "integer", nullable: false),
                    EmployeeId = table.Column<int>(type: "integer", nullable: false),
                    Result = table.Column<int>(type: "integer", nullable: false),
                    DateSend = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    RouteStageNextId = table.Column<int>(type: "integer", nullable: true),
                    MyProperty = table.Column<int>(type: "integer", nullable: false),
                    FlagInitiation = table.Column<bool>(type: "boolean", nullable: false),
                    InitiatorDocumentStateId = table.Column<int>(type: "integer", nullable: false),
                    PreviousDocumentStateId = table.Column<int>(type: "integer", nullable: false),
                    Comment = table.Column<string>(type: "text", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentStateTransactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DocumentStateTransactions_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumentStateTransactions_DocumentStateTransactions_Initiat~",
                        column: x => x.InitiatorDocumentStateId,
                        principalTable: "DocumentStateTransactions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumentStateTransactions_DocumentStateTransactions_Previou~",
                        column: x => x.PreviousDocumentStateId,
                        principalTable: "DocumentStateTransactions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumentStateTransactions_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumentStateTransactions_RouteStages_RouteStageNextId",
                        column: x => x.RouteStageNextId,
                        principalTable: "RouteStages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DocumentStates",
                columns: table => new
                {
                    DocumentId = table.Column<int>(type: "integer", nullable: false),
                    EmployeeId = table.Column<int>(type: "integer", nullable: false),
                    Result = table.Column<int>(type: "integer", nullable: false),
                    DateSend = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    RouteStageNextId = table.Column<int>(type: "integer", nullable: true),
                    MyProperty = table.Column<int>(type: "integer", nullable: false),
                    FlagInitiation = table.Column<bool>(type: "boolean", nullable: false),
                    InitiatorDocumentStateId = table.Column<int>(type: "integer", nullable: false),
                    PreviousDocumentStateId = table.Column<int>(type: "integer", nullable: false),
                    Comment = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentStates", x => x.DocumentId);
                    table.ForeignKey(
                        name: "FK_DocumentStates_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumentStates_DocumentStateTransactions_InitiatorDocumentS~",
                        column: x => x.InitiatorDocumentStateId,
                        principalTable: "DocumentStateTransactions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumentStates_DocumentStateTransactions_PreviousDocumentSt~",
                        column: x => x.PreviousDocumentStateId,
                        principalTable: "DocumentStateTransactions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumentStates_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumentStates_RouteStages_RouteStageNextId",
                        column: x => x.RouteStageNextId,
                        principalTable: "RouteStages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 861, DateTimeKind.Local).AddTicks(1359));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 862, DateTimeKind.Local).AddTicks(6967));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 862, DateTimeKind.Local).AddTicks(6988));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(2690));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3836));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3846));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3847));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3848));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3850));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3851));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3852));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3854));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3855));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3856));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3857));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3859));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3860));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(3861));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 865, DateTimeKind.Local).AddTicks(9502));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 866, DateTimeKind.Local).AddTicks(1056));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 866, DateTimeKind.Local).AddTicks(1065));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 866, DateTimeKind.Local).AddTicks(1066));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 866, DateTimeKind.Local).AddTicks(1068));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 866, DateTimeKind.Local).AddTicks(1069));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 866, DateTimeKind.Local).AddTicks(1070));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 866, DateTimeKind.Local).AddTicks(1072));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(7457));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(8038));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(8046));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(8047));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(8048));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(8050));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(8051));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(8052));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 864, DateTimeKind.Local).AddTicks(8053));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 865, DateTimeKind.Local).AddTicks(5399));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 20, 13, 45, 865, DateTimeKind.Local).AddTicks(5971));

            migrationBuilder.CreateIndex(
                name: "IX_DocumentStates_EmployeeId",
                table: "DocumentStates",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentStates_InitiatorDocumentStateId",
                table: "DocumentStates",
                column: "InitiatorDocumentStateId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DocumentStates_PreviousDocumentStateId",
                table: "DocumentStates",
                column: "PreviousDocumentStateId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DocumentStates_RouteStageNextId",
                table: "DocumentStates",
                column: "RouteStageNextId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentStateTransactions_DocumentId",
                table: "DocumentStateTransactions",
                column: "DocumentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DocumentStateTransactions_EmployeeId",
                table: "DocumentStateTransactions",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentStateTransactions_InitiatorDocumentStateId",
                table: "DocumentStateTransactions",
                column: "InitiatorDocumentStateId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DocumentStateTransactions_PreviousDocumentStateId",
                table: "DocumentStateTransactions",
                column: "PreviousDocumentStateId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DocumentStateTransactions_RouteStageNextId",
                table: "DocumentStateTransactions",
                column: "RouteStageNextId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DocumentStates");

            migrationBuilder.DropTable(
                name: "DocumentStateTransactions");

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 895, DateTimeKind.Local).AddTicks(4727));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 896, DateTimeKind.Local).AddTicks(8152));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 896, DateTimeKind.Local).AddTicks(8170));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(4025));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5170));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5180));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5182));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5184));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5185));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5187));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5189));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5190));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5192));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5194));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5195));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5197));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5198));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5200));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 900, DateTimeKind.Local).AddTicks(1060));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 900, DateTimeKind.Local).AddTicks(2695));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 900, DateTimeKind.Local).AddTicks(2706));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 900, DateTimeKind.Local).AddTicks(2708));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 900, DateTimeKind.Local).AddTicks(2710));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 900, DateTimeKind.Local).AddTicks(2712));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 900, DateTimeKind.Local).AddTicks(2714));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 900, DateTimeKind.Local).AddTicks(2715));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(8647));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(9238));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(9247));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(9249));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(9250));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(9252));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(9254));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(9256));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(9257));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 899, DateTimeKind.Local).AddTicks(6867));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 899, DateTimeKind.Local).AddTicks(7444));
        }
    }
}
