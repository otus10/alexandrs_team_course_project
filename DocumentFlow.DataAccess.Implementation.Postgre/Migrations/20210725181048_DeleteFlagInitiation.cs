﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DocumentFlow.DataAccess.Implementation.Postgre.Migrations
{
    public partial class DeleteFlagInitiation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FlagInitiation",
                table: "DocumentStateTransactions");

            migrationBuilder.DropColumn(
                name: "FlagInitiation",
                table: "DocumentStates");

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 664, DateTimeKind.Local).AddTicks(9417));

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 665, DateTimeKind.Local).AddTicks(869));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 653, DateTimeKind.Local).AddTicks(906));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 656, DateTimeKind.Local).AddTicks(7345));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 656, DateTimeKind.Local).AddTicks(7477));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 660, DateTimeKind.Local).AddTicks(3777));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 660, DateTimeKind.Local).AddTicks(5950));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 660, DateTimeKind.Local).AddTicks(5968));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 660, DateTimeKind.Local).AddTicks(5973));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 660, DateTimeKind.Local).AddTicks(5977));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 660, DateTimeKind.Local).AddTicks(5980));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 660, DateTimeKind.Local).AddTicks(5983));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 660, DateTimeKind.Local).AddTicks(5987));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 660, DateTimeKind.Local).AddTicks(5990));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 660, DateTimeKind.Local).AddTicks(5993));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 660, DateTimeKind.Local).AddTicks(5997));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 660, DateTimeKind.Local).AddTicks(6000));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 660, DateTimeKind.Local).AddTicks(6003));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 660, DateTimeKind.Local).AddTicks(6007));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 660, DateTimeKind.Local).AddTicks(6010));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 664, DateTimeKind.Local).AddTicks(1581));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 664, DateTimeKind.Local).AddTicks(3785));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 664, DateTimeKind.Local).AddTicks(3801));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 664, DateTimeKind.Local).AddTicks(3805));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 664, DateTimeKind.Local).AddTicks(3809));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 664, DateTimeKind.Local).AddTicks(3812));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 664, DateTimeKind.Local).AddTicks(3816));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 664, DateTimeKind.Local).AddTicks(3819));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 661, DateTimeKind.Local).AddTicks(3973));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 661, DateTimeKind.Local).AddTicks(4810));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 661, DateTimeKind.Local).AddTicks(4825));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 661, DateTimeKind.Local).AddTicks(4830));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 661, DateTimeKind.Local).AddTicks(4836));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 661, DateTimeKind.Local).AddTicks(4845));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 661, DateTimeKind.Local).AddTicks(4852));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 661, DateTimeKind.Local).AddTicks(4857));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 661, DateTimeKind.Local).AddTicks(4862));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 663, DateTimeKind.Local).AddTicks(1282));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 7, 25, 21, 10, 47, 663, DateTimeKind.Local).AddTicks(2135));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "FlagInitiation",
                table: "DocumentStateTransactions",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "FlagInitiation",
                table: "DocumentStates",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 204, DateTimeKind.Local).AddTicks(450));

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 204, DateTimeKind.Local).AddTicks(1040));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 200, DateTimeKind.Local).AddTicks(9567));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 201, DateTimeKind.Local).AddTicks(6106));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 201, DateTimeKind.Local).AddTicks(6119));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(6727));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7402));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7408));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7409));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7410));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7411));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7412));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7413));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7414));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7415));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7416));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7417));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7418));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7419));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(7420));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(7917));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(8783));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(8788));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(8789));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(8790));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(8791));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(8793));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(8794));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 202, DateTimeKind.Local).AddTicks(9824));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(175));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(179));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(180));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(181));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(182));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(183));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(184));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(185));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(5176));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 26, 19, 12, 17, 203, DateTimeKind.Local).AddTicks(5509));
        }
    }
}
