﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DocumentFlow.DataAccess.Implementation.Postgre.Migrations
{
    public partial class DocumentStateTransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateSend",
                table: "DocumentStateTransactions");

            migrationBuilder.DropColumn(
                name: "MyProperty",
                table: "DocumentStateTransactions");

            migrationBuilder.DropColumn(
                name: "MyProperty",
                table: "DocumentStates");

            migrationBuilder.RenameColumn(
                name: "DateSend",
                table: "DocumentStates",
                newName: "DateCreated");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateUpdated",
                table: "DocumentStates",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumns: new[] { "DocumentTypeId", "RouteId" },
                keyValues: new object[] { 1, 1 },
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(6085));

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumns: new[] { "DocumentTypeId", "RouteId" },
                keyValues: new object[] { 3, 2 },
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(6846));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 200, DateTimeKind.Local).AddTicks(8173));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 201, DateTimeKind.Local).AddTicks(6218));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 201, DateTimeKind.Local).AddTicks(6231));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 202, DateTimeKind.Local).AddTicks(9377));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(248));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(293));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(295));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(296));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(298));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(299));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(300));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(301));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(302));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(304));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(305));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(306));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(307));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(308));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(3133));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(4281));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(4287));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(4288));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(4289));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(4291));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(4292));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(4293));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(3193));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(3650));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(3654));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(3656));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(3657));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(3658));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(3659));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(3660));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(3661));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 203, DateTimeKind.Local).AddTicks(9750));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 16, 13, 43, 54, 204, DateTimeKind.Local).AddTicks(182));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateUpdated",
                table: "DocumentStates");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "DocumentStates",
                newName: "DateSend");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateSend",
                table: "DocumentStateTransactions",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "MyProperty",
                table: "DocumentStateTransactions",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MyProperty",
                table: "DocumentStates",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumns: new[] { "DocumentTypeId", "RouteId" },
                keyValues: new object[] { 1, 1 },
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(5703));

            migrationBuilder.UpdateData(
                table: "DocumentTemplates",
                keyColumns: new[] { "DocumentTypeId", "RouteId" },
                keyValues: new object[] { 3, 2 },
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(6457));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 808, DateTimeKind.Local).AddTicks(7844));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 809, DateTimeKind.Local).AddTicks(5564));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 809, DateTimeKind.Local).AddTicks(5578));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(8804));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9682));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9687));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9689));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9690));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9691));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9692));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9694));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9695));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9696));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9697));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9698));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9699));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9701));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 810, DateTimeKind.Local).AddTicks(9702));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(2731));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3897));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3901));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3903));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3904));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3905));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3907));

            migrationBuilder.UpdateData(
                table: "RouteBodies",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 812, DateTimeKind.Local).AddTicks(3908));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(2576));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3032));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3036));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3037));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3038));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3039));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3041));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3042));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(3043));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(9223));

            migrationBuilder.UpdateData(
                table: "Routes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 15, 18, 12, 3, 811, DateTimeKind.Local).AddTicks(9660));
        }
    }
}
