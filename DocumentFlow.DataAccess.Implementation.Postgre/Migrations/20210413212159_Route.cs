﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DocumentFlow.DataAccess.Implementation.Postgre.Migrations
{
    public partial class Route : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Routes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Routes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RouteBodies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RouteId = table.Column<int>(type: "integer", nullable: false),
                    RouteStageId = table.Column<int>(type: "integer", nullable: false),
                    RouteStageOrder = table.Column<int>(type: "integer", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RouteBodies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RouteBodies_Routes_RouteId",
                        column: x => x.RouteId,
                        principalTable: "Routes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RouteBodies_RouteStages_RouteStageId",
                        column: x => x.RouteStageId,
                        principalTable: "RouteStages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 895, DateTimeKind.Local).AddTicks(4727));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 896, DateTimeKind.Local).AddTicks(8152));

            migrationBuilder.InsertData(
                table: "DocumentTypes",
                columns: new[] { "Id", "Code", "DateCreated", "DateUpdated", "NameEn" },
                values: new object[] { 3, "joboffer", new DateTime(2021, 4, 14, 0, 21, 58, 896, DateTimeKind.Local).AddTicks(8170), null, "Job Offer" });

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(4025));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5170));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5180));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5182));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5184));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5185));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5187));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5189));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5190));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5192));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5194));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5195));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5197));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5198));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(5200));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(8647));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(9238));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(9247));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(9249));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(9250));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(9252));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(9254));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(9256));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 14, 0, 21, 58, 898, DateTimeKind.Local).AddTicks(9257));

            migrationBuilder.InsertData(
                table: "Routes",
                columns: new[] { "Id", "DateCreated", "DateUpdated", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 4, 14, 0, 21, 58, 899, DateTimeKind.Local).AddTicks(6867), null, "Payment Order" },
                    { 2, new DateTime(2021, 4, 14, 0, 21, 58, 899, DateTimeKind.Local).AddTicks(7444), null, "New Employee offer" }
                });

            migrationBuilder.InsertData(
                table: "RouteBodies",
                columns: new[] { "Id", "DateCreated", "DateUpdated", "RouteId", "RouteStageId", "RouteStageOrder" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 4, 14, 0, 21, 58, 900, DateTimeKind.Local).AddTicks(1060), null, 1, 3, 1 },
                    { 2, new DateTime(2021, 4, 14, 0, 21, 58, 900, DateTimeKind.Local).AddTicks(2695), null, 1, 4, 2 },
                    { 3, new DateTime(2021, 4, 14, 0, 21, 58, 900, DateTimeKind.Local).AddTicks(2706), null, 1, 5, 3 },
                    { 4, new DateTime(2021, 4, 14, 0, 21, 58, 900, DateTimeKind.Local).AddTicks(2708), null, 1, 6, 4 },
                    { 5, new DateTime(2021, 4, 14, 0, 21, 58, 900, DateTimeKind.Local).AddTicks(2710), null, 1, 7, 5 },
                    { 6, new DateTime(2021, 4, 14, 0, 21, 58, 900, DateTimeKind.Local).AddTicks(2712), null, 2, 8, 1 },
                    { 7, new DateTime(2021, 4, 14, 0, 21, 58, 900, DateTimeKind.Local).AddTicks(2714), null, 2, 2, 2 },
                    { 8, new DateTime(2021, 4, 14, 0, 21, 58, 900, DateTimeKind.Local).AddTicks(2715), null, 2, 6, 3 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_RouteBodies_RouteId",
                table: "RouteBodies",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_RouteBodies_RouteStageId",
                table: "RouteBodies",
                column: "RouteStageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RouteBodies");

            migrationBuilder.DropTable(
                name: "Routes");

            migrationBuilder.DeleteData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 295, DateTimeKind.Local).AddTicks(5617));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 296, DateTimeKind.Local).AddTicks(9735));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(4576));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5732));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5741));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5743));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5744));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5745));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5747));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5748));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5749));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5750));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5752));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5753));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5754));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5756));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5757));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 313, DateTimeKind.Local).AddTicks(460));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 313, DateTimeKind.Local).AddTicks(1060));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 313, DateTimeKind.Local).AddTicks(1069));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 313, DateTimeKind.Local).AddTicks(1070));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 313, DateTimeKind.Local).AddTicks(1072));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 313, DateTimeKind.Local).AddTicks(1074));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 313, DateTimeKind.Local).AddTicks(1075));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 313, DateTimeKind.Local).AddTicks(1077));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 313, DateTimeKind.Local).AddTicks(1078));
        }
    }
}
