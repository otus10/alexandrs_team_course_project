﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DocumentFlow.DataAccess.Implementation.Postgre.Migrations
{
    public partial class DocumentFixInitiator : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employees_Documents_DocumentId",
                table: "Employees");

            migrationBuilder.DropTable(
                name: "DocumentInitiators");

            migrationBuilder.DropIndex(
                name: "IX_Employees_DocumentId",
                table: "Employees");

            migrationBuilder.DropColumn(
                name: "DocumentId",
                table: "Employees");

            migrationBuilder.AddColumn<int>(
                name: "InitiatorId",
                table: "Documents",
                type: "integer",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 295, DateTimeKind.Local).AddTicks(5617));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 296, DateTimeKind.Local).AddTicks(9735));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(4576));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5732));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5741));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5743));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5744));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5745));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5747));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5748));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5749));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5750));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5752));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5753));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5754));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5756));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 312, DateTimeKind.Local).AddTicks(5757));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 313, DateTimeKind.Local).AddTicks(460));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 313, DateTimeKind.Local).AddTicks(1060));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 313, DateTimeKind.Local).AddTicks(1069));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 313, DateTimeKind.Local).AddTicks(1070));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 313, DateTimeKind.Local).AddTicks(1072));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 313, DateTimeKind.Local).AddTicks(1074));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 313, DateTimeKind.Local).AddTicks(1075));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 313, DateTimeKind.Local).AddTicks(1077));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 9, 14, 21, 13, 313, DateTimeKind.Local).AddTicks(1078));

            migrationBuilder.CreateIndex(
                name: "IX_Documents_InitiatorId",
                table: "Documents",
                column: "InitiatorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_Employees_InitiatorId",
                table: "Documents",
                column: "InitiatorId",
                principalTable: "Employees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Documents_Employees_InitiatorId",
                table: "Documents");

            migrationBuilder.DropIndex(
                name: "IX_Documents_InitiatorId",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "InitiatorId",
                table: "Documents");

            migrationBuilder.AddColumn<int>(
                name: "DocumentId",
                table: "Employees",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "DocumentInitiators",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    DocumentId = table.Column<int>(type: "integer", nullable: false),
                    InitiatorId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentInitiators", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DocumentInitiators_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumentInitiators_Employees_InitiatorId",
                        column: x => x.InitiatorId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 378, DateTimeKind.Local).AddTicks(8873));

            migrationBuilder.UpdateData(
                table: "DocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 380, DateTimeKind.Local).AddTicks(2617));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 396, DateTimeKind.Local).AddTicks(5158));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 396, DateTimeKind.Local).AddTicks(6307));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 396, DateTimeKind.Local).AddTicks(6317));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 396, DateTimeKind.Local).AddTicks(6318));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 396, DateTimeKind.Local).AddTicks(6319));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 396, DateTimeKind.Local).AddTicks(6321));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 396, DateTimeKind.Local).AddTicks(6322));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 396, DateTimeKind.Local).AddTicks(6323));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 396, DateTimeKind.Local).AddTicks(6325));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 396, DateTimeKind.Local).AddTicks(6326));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 396, DateTimeKind.Local).AddTicks(6327));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 396, DateTimeKind.Local).AddTicks(6328));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 396, DateTimeKind.Local).AddTicks(6330));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 396, DateTimeKind.Local).AddTicks(6331));

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 15,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 396, DateTimeKind.Local).AddTicks(6332));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: -1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 396, DateTimeKind.Local).AddTicks(9755));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 397, DateTimeKind.Local).AddTicks(339));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 397, DateTimeKind.Local).AddTicks(348));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 397, DateTimeKind.Local).AddTicks(350));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 397, DateTimeKind.Local).AddTicks(351));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 397, DateTimeKind.Local).AddTicks(352));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 397, DateTimeKind.Local).AddTicks(353));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 397, DateTimeKind.Local).AddTicks(355));

            migrationBuilder.UpdateData(
                table: "RouteStages",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreated",
                value: new DateTime(2021, 4, 7, 22, 0, 58, 397, DateTimeKind.Local).AddTicks(356));

            migrationBuilder.CreateIndex(
                name: "IX_Employees_DocumentId",
                table: "Employees",
                column: "DocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentInitiators_DocumentId",
                table: "DocumentInitiators",
                column: "DocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentInitiators_InitiatorId",
                table: "DocumentInitiators",
                column: "InitiatorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_Documents_DocumentId",
                table: "Employees",
                column: "DocumentId",
                principalTable: "Documents",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
