﻿using DocumentFlow.Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace DocumentFlow.DataAccess.Implementation.Postgre
{
    internal static class DataSeed
    {
        public static void Seed(this ModelBuilder builder)
        {
            ConfigureEmployeeRouteStage(builder);
            ConfigureDocumentStateTransaction(builder);
            ConfigureDocumentState(builder);
            HasDataDocumentType(builder);
            HasDataEmployee(builder);
            HasDataRouteStage(builder);
            HasDataEmployeeRouteStage(builder);
            HasDataRoute(builder);
            HasDataRouteBody(builder);
            HasDataDocumentTemplate(builder);
        }

        private static void ConfigureEmployeeRouteStage(ModelBuilder builder)
        {
            builder.Entity<EmployeeRouteStage>().HasKey(ers => new { ers.EmployeeId, ers.RouteStageId });

            builder.Entity<EmployeeRouteStage>()
                .HasOne(ers => ers.Employee)
                .WithMany(e => e.EmployeeRouteStages)
                .HasForeignKey(ers => ers.EmployeeId);

            builder.Entity<EmployeeRouteStage>()
                .HasOne(ers => ers.RouteStage)
                .WithMany(rs => rs.EmployeeRouteStages)
                .HasForeignKey(ers => ers.RouteStageId);
        }

        private static void ConfigureDocumentStateTransaction(ModelBuilder builder)
        {
            builder.Entity<DocumentStateTransaction>()
                .HasOne(fk => fk.Document)
                .WithMany()
                .IsRequired();
            builder.Entity<DocumentStateTransaction>()
                .HasOne(fk => fk.InitiatorDocumentState)
                .WithMany()
                .IsRequired(false);
            builder.Entity<DocumentStateTransaction>()
                .HasOne(fk => fk.PreviousDocumentState)
                .WithMany()
                .IsRequired(false);
            builder.Entity<DocumentStateTransaction>()
                .Property(m => m.RouteStageOrder)
                .IsRequired(false);
            builder.Entity<DocumentStateTransaction>()
                .Property(m => m.RouteStageNextId)
                .IsRequired(false);
        }

        private static void ConfigureDocumentState(ModelBuilder builder)
        {
            builder.Entity<DocumentState>()
                .HasOne(fk => fk.Document)
                .WithOne()
                .IsRequired();
            builder.Entity<DocumentState>()
                .HasOne(fk => fk.InitiatorDocumentState)
                .WithOne()
                .IsRequired();
            builder.Entity<DocumentState>()
                .HasOne(fk => fk.PreviousDocumentState)
                .WithOne()
                .IsRequired(false);
            builder.Entity<DocumentState>()
                .HasOne(fk => fk.CurrentDocumentState)
                .WithOne()
                .IsRequired();
            builder.Entity<DocumentState>()
                .Property(m => m.RouteStageOrder)
                .IsRequired(false);
            builder.Entity<DocumentState>()
                .Property(m => m.RouteStageNextId)
                .IsRequired(false);
        }

        private static void HasDataEmployee(ModelBuilder builder)
        {
            builder.Entity<Employee>().HasData(
                new Employee
                {
                    Id = 1,
                    FullName = "Шилов Варлаам Андреевич",
                    JobTitle = JobTitle.Developer,
                },
                new Employee
                {
                    Id = 2,
                    FullName = "Аксёнов Авраам Робертович",
                    JobTitle = JobTitle.Developer,
                },
                new Employee
                {
                    Id = 3,
                    FullName = "Веселов Рудольф Степанович",
                    JobTitle = JobTitle.TeamLead,
                },
                new Employee
                {
                    Id = 4,
                    FullName = "Сидоров Семен Эдуардович",
                    JobTitle = JobTitle.ProjectManager,
                },
                new Employee
                {
                    Id = 5,
                    FullName = "Копылов Ефим Наумович",
                    JobTitle = JobTitle.ChiefDevelopmentDepartment,
                },
                new Employee
                {
                    Id = 6,
                    FullName = "Гурьев Агафон Феликсович",
                    JobTitle = JobTitle.FinancialDirector,
                },
                new Employee
                {
                    Id = 7,
                    FullName = "Баранов Назарий Куприянович",
                    JobTitle = JobTitle.GeneralDirector,
                },
                new Employee
                {
                    Id = 8,
                    FullName = "Кабанова Кристина Лаврентьевна",
                    JobTitle = JobTitle.HRManager,
                },
                new Employee
                {
                    Id = 9,
                    FullName = "Журавлёва Земфира Андреевна",
                    JobTitle = JobTitle.ChiefHR,
                },
                new Employee
                {
                    Id = 10,
                    FullName = "Александрова Ульяна Якуновна",
                    JobTitle = JobTitle.Accountant,
                },
                new Employee
                {
                    Id = 11,
                    FullName = "Филатова Гелана Демьяновна",
                    JobTitle = JobTitle.Accountant,
                },
                new Employee
                {
                    Id = 12,
                    FullName = "Корнилова Ирма Рудольфовна",
                    JobTitle = JobTitle.Accountant,
                },
                new Employee
                {
                    Id = 13,
                    FullName = "Лаврентьева Амелия Игнатьевна",
                    JobTitle = JobTitle.SeniorAccountant,
                },
                new Employee
                {
                    Id = 14,
                    FullName = "Филатова Ирина Денисовна",
                    JobTitle = JobTitle.SeniorAccountant,
                },
                new Employee
                {
                    Id = 15,
                    FullName = "Богданова Людмила Авксентьевна",
                    JobTitle = JobTitle.ChiefAccountant,
                });
        }

        private static void HasDataRouteStage(ModelBuilder builder)
        {
            builder.Entity<RouteStage>().HasData(
               new RouteStage
               {
                   Id = -1,
                   Description = "Zero level",
               },
               new RouteStage
               {
                   Id = 1,
                   Description = "Chief of development department",
               },
               new RouteStage
               {
                   Id = 2,
                   Description = "Chief of HR",
               },
               new RouteStage
               {
                   Id = 3,
                   Description = "Accountants",
               },
               new RouteStage
               {
                   Id = 4,
                   Description = "Seniors accountants",
               },
               new RouteStage
               {
                   Id = 5,
                   Description = "Chief of accountant",
               },
               new RouteStage
               {
                   Id = 6,
                   Description = "Financial Director",
               },
               new RouteStage
               {
                   Id = 7,
                   Description = "General Director",
               },
               new RouteStage
               {
                   Id = 8,
                   Description = "HR Manager",
               });
        }

        private static void HasDataEmployeeRouteStage(ModelBuilder builder)
        {
            builder.Entity<EmployeeRouteStage>().HasData(
                new EmployeeRouteStage
                {
                    EmployeeId = 1,
                    RouteStageId = -1,
                },
                new EmployeeRouteStage
                {
                    EmployeeId = 2,
                    RouteStageId = -1,
                },
                new EmployeeRouteStage
                {
                    EmployeeId = 3,
                    RouteStageId = -1,
                },
                new EmployeeRouteStage
                {
                    EmployeeId = 4,
                    RouteStageId = -1,
                },
                new EmployeeRouteStage
                {
                    EmployeeId = 5,
                    RouteStageId = 1,
                },
                new EmployeeRouteStage
                {
                    EmployeeId = 6,
                    RouteStageId = 6,
                },
                new EmployeeRouteStage
                {
                    EmployeeId = 7,
                    RouteStageId = 7,
                },
                new EmployeeRouteStage
                {
                    EmployeeId = 8,
                    RouteStageId = -1,
                },
                new EmployeeRouteStage
                {
                    EmployeeId = 8,
                    RouteStageId = 8,
                },
                new EmployeeRouteStage
                {
                    EmployeeId = 9,
                    RouteStageId = 2,
                },
                new EmployeeRouteStage
                {
                    EmployeeId = 10,
                    RouteStageId = 3,
                },
                new EmployeeRouteStage
                {
                    EmployeeId = 11,
                    RouteStageId = 3,
                },
                new EmployeeRouteStage
                {
                    EmployeeId = 12,
                    RouteStageId = 3,
                },
                new EmployeeRouteStage
                {
                    EmployeeId = 13,
                    RouteStageId = 4,
                },
                new EmployeeRouteStage
                {
                    EmployeeId = 14,
                    RouteStageId = 4,
                },
                new EmployeeRouteStage
                {
                    EmployeeId = 15,
                    RouteStageId = 5,
                });
        }
        private static void HasDataDocumentType(ModelBuilder builder)
        {
            builder.Entity<DocumentType>().HasData(
                new DocumentType
                {
                    Id = 1,
                    Code = "purord",
                    NameEn = "Purchase Order",
                },
                new DocumentType
                {
                    Id = 2,
                    Code = "payord",
                    NameEn = "Payment Order"
                },
                new DocumentType
                {
                    Id = 3,
                    Code = "joboffer",
                    NameEn = "Job Offer"
                });
        }
        private static void HasDataRoute(ModelBuilder builder)
        {
            builder.Entity<Route>().HasData(
                new Route
                {
                    Id = 1,
                    Name = "Payment Order",
                },
                new Route
                {
                    Id = 2,
                    Name = "New Employee offer",
                }
            );
        }
        private static void HasDataRouteBody(ModelBuilder builder)
        {
            builder.Entity<RouteBody>().HasData(
                new RouteBody
                {
                    Id = 1,
                    RouteId = 1,
                    RouteStageId = 3,
                    RouteStageOrder = 1,
                },
                new RouteBody
                {
                    Id = 2,
                    RouteId = 1,
                    RouteStageId = 4,
                    RouteStageOrder = 2,
                },
                new RouteBody
                {
                    Id = 3,
                    RouteId = 1,
                    RouteStageId = 5,
                    RouteStageOrder = 3,
                },
                new RouteBody
                {
                    Id = 4,
                    RouteId = 1,
                    RouteStageId = 6,
                    RouteStageOrder = 4,
                },
                new RouteBody
                {
                    Id = 5,
                    RouteId = 1,
                    RouteStageId = 7,
                    RouteStageOrder = 5,
                },
                new RouteBody
                {
                    Id = 6,
                    RouteId = 2,
                    RouteStageId = 8,
                    RouteStageOrder = 1,
                },
                new RouteBody
                {
                    Id = 7,
                    RouteId = 2,
                    RouteStageId = 2,
                    RouteStageOrder = 2,
                },
                new RouteBody
                {
                    Id = 8,
                    RouteId = 2,
                    RouteStageId = 6,
                    RouteStageOrder = 3,
                });

        }

        private static void HasDataDocumentTemplate(ModelBuilder builder)
        {
            builder.Entity<DocumentTemplate>().HasData(
                new DocumentTemplate
                {
                    Id = 1,
                    DocumentTypeId = 1,
                    RouteId = 1,
                },
                new DocumentTemplate
                {
                    Id = 2,
                    DocumentTypeId = 3,
                    RouteId = 2,
                });
        }
    }
}
