﻿using DocumentFlow.Utils.Modules;
using DocumentFlow.DataAccess.Interfaces.DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentFlow.DataAccess.Implementation.Postgre
{
   public class DataAccessModule : Module
   {
      public override void Load(IServiceCollection services)
      {
         services.AddDbContext<IDbContext, DataContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("PostgreConnection")
                ,assembly => assembly.MigrationsAssembly(typeof(DataAccessModule).Assembly.GetName().Name)));
        }
   }
}
;