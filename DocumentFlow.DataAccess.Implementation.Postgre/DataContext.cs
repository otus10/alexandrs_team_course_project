﻿using DocumentFlow.Entities.Models;
using DocumentFlow.DataAccess.Interfaces.DataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DocumentFlow.DataAccess.Implementation.Postgre
{
    public class DataContext : DbContext, IDbContext
    {
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<DocumentAttachment> DocumentAttachments { get; set; }
        public DbSet<DocumentState> DocumentStates { get; set; }
        public DbSet<DocumentStateTransaction> DocumentStateTransactions { get; set; }
        public DbSet<DocumentTemplate> DocumentTemplates { get; set; }
        public DbSet<DocumentType> DocumentTypes { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeeRouteStage> EmployeeRouteStages { get; set; }
        public DbSet<Route> Routes { get; set; }
        public DbSet<RouteBody> RouteBodies { get; set; }
        public DbSet<RouteStage> RouteStages { get; set; }


        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public async Task<int> SaveChangesAsync()
        {
            return await base.SaveChangesAsync();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Seed();
        }
        //public DbSet<T> DbSet<T>() where T : class
        //{
        //   return Set<T>();
        //}

        //public new IQueryable<T> Query<T>() where T : class
        //{
        //   return Set<T>();
        //}
    }
}
