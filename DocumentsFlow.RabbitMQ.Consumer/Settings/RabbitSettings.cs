﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocumentsFlow.RabbitMQ.Consumer.Settings
{
    public class RabbitSettings
    {
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string RouteKey { get; set; }
        public bool Durable { get; set; }
        public string Exchange { get; set; }
        public string ExchangeType { get; set; }
        public string HostName { get; set; }
        public string QueueName { get; set; }
        public bool AutoDelete { get; set; }
        public bool Exclusive { get; set; }
    }
}
