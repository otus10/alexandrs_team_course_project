﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DocumentsFlow.RabbitMQ.Consumer.Settings;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace DocumentsFlow.RabbitMQ.Consumer.Listeners.Base
{
    public class RabbitListener : IHostedService
    {
        private readonly IConnection connection;
        private readonly IModel channel;
        private readonly IOptions<RabbitSettings> _options;
        public RabbitListener(IOptions<RabbitSettings> options)
        {
            _options = options;
            try
            {
                var factory = new ConnectionFactory()
                {
                    HostName = _options.Value.HostName,
                    UserName = _options.Value.Username,
                    Password = _options.Value.Password,
                    Port = _options.Value.Port,
                };
                this.connection = factory.CreateConnection();
                this.channel = connection.CreateModel();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"RabbitListener init error, ex:{ex.Message}");
            }      
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            Register();
            return Task.CompletedTask;
        }

        protected string RouteKey;
        protected string QueueName;

        // How to process messages
        public virtual bool Process(string message)
        {
            throw new NotImplementedException();
        }

        // Registered consumer monitoring here
        public void Register()
        {
            Console.WriteLine($"RabbitListener register, routeKey:{RouteKey}");
            channel.ExchangeDeclare(exchange: _options.Value.Exchange, 
                                    type: _options.Value.ExchangeType, 
                                    durable: _options.Value.Durable);
            channel.QueueDeclare(queue: QueueName, 
                                 exclusive: _options.Value.Exclusive, 
                                 durable: _options.Value.Durable, 
                                 autoDelete: _options.Value.AutoDelete);
            channel.QueueBind(queue: QueueName,
                              exchange: _options.Value.Exchange,
                              routingKey: RouteKey);
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body;
                var message = Encoding.UTF8.GetString(body.ToArray());
                var result = Process(message);
                if (result)
                {
                    channel.BasicAck(ea.DeliveryTag, false);
                }
            };
            channel.BasicConsume(queue: QueueName, consumer: consumer);
        }

        public void DeRegister()
        {
            this.connection.Close();
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            this.connection.Close();
            return Task.CompletedTask;
        }
    }

}
