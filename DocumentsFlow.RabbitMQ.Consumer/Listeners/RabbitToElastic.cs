﻿using System;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using DocumentsFlow.RabbitMQ.Consumer.Listeners.Base;
using System.IO;
using DocumentsFlow.RabbitMQ.Consumer.Settings;
using Serilog;

namespace DocumentsFlow.RabbitMQ.Consumer.Listeners
{
    public class RabbitToElastic : RabbitListener
    {
        private readonly IOptions<RabbitSettings> _options;
        public RabbitToElastic(IOptions<RabbitSettings> options) : base(options)
        {
            _options = options;
            base.RouteKey = _options.Value.RouteKey;
            base.QueueName = _options.Value.QueueName; 
        }
        public override bool Process(string message)
        {
            Log.Logger.Information(message);
            return true;
        }
    }
}
