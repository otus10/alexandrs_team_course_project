﻿using System;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using DocumentsFlow.RabbitMQ.Consumer.Listeners.Base;
using System.IO;
using DocumentsFlow.RabbitMQ.Consumer.Settings;

namespace DocumentsFlow.RabbitMQ.Consumer.Listeners
{
    public class RabbitToConsole : RabbitListener
    {
        private readonly IOptions<RabbitSettings> _options;
        public RabbitToConsole(IOptions<RabbitSettings> options) : base(options)
        {
            _options = options;
            base.RouteKey = _options.Value.RouteKey;
            base.QueueName = _options.Value.QueueName; 
        }
        public override bool Process(string message)
        {     
            Console.WriteLine(message);
            return true;
        }
    }
}
