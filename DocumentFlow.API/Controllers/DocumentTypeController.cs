﻿using DocumentFlow.UseCases.Handlers.Documents.Commands;
using DocumentFlow.UserCases.Handlers.Documents.Dto;
using DocumentFlow.UserCases.Handlers.Documents.Queries.GetDocumentType;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DocumentFlow.API.Controllers
{
    [Route("api/documentTypes")]
    [ApiController]
    public class DocumentTypesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public DocumentTypesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        // GET api/documentTypes/5
        [HttpGet("{Id}")]
        public async Task<ActionResult<DocumentTypeDto>> Get([FromRoute] GetDocumentType.Request request)
        {
            return await _mediator.Send(request);
        }

        // POST api/documentTypes
        [HttpPost]
        public async Task Post([FromBody] CreateDocumentType.Command command)
        {
            await _mediator.Send(command);
        }
    }
}