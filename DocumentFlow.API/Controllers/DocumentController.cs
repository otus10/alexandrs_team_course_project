﻿using DocumentFlow.UseCases.Handlers.Documents.Commands;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using DocumentFlow.UseCases.Handlers.Documents.Queries;
using DocumentFlow.UserCases.Handlers.Documents.Dto;
using Microsoft.Extensions.Logging;

namespace DocumentFlow.API.Controllers
{
    [Route("api/documents/")]
    [ApiController]
    public class DocumentController : ControllerBase
    {
        private readonly IMediator _mediator;
        public DocumentController(IMediator mediator, ILogger<DocumentController> logger)
        {
            _mediator = mediator;
        }

        // POST api/documents
        [HttpPost]
        [Route("Create")]
        public async Task Post([FromForm] CreateDocument.Command command)
        {
            await _mediator.Send(command);
        }

        // PUT api/documents/5/:startRoute
        [HttpPut]
        [Route("{DocumentId}/:startRoute")]
        public async Task StartRoute([FromRoute] StartDocumentRoute.Command command)
        {
            await _mediator.Send(command);
        }

        // PUT api/documents/5/:approve
        [HttpPut]
        [Route("{DocumentId}/:approve")]
        public async Task Approve([FromRoute] ApproveDocument.Command command)
        {
            await _mediator.Send(command);
        }

        // PUT api/documents/5/:reject
        [HttpPut]
        [Route("{DocumentId}/:reject")]
        public async Task Reject([FromRoute] RejectDocument.Command command)
        {
            await _mediator.Send(command);
        }

        // PUT api/documents/5/:cancel
        [HttpPut]
        [Route("{DocumentId}/:cancel")]
        public async Task Cancel([FromRoute] CancelDocument.Command command)
        {
            await _mediator.Send(command);
        }

        // PUT api/documents/5/:onHold
        [HttpPut]
        [Route("{DocumentId}/:onHold")]
        public async Task OnHold([FromRoute] OnHoldDocument.Command command)
        {
            await _mediator.Send(command);
        }

        // POST api/documents/files
        [HttpPost]
        [Route("files")]
        public async Task AddFiles([FromForm] AddFiles.Command command)
        {
            await _mediator.Send(command);
        }

        // GET api/documents?EmployeeId=5&Result=5&DocumentTypeId=5&DocumentStateId=5
        [HttpGet]
        public async Task<ActionResult<List<DocumentDto>>> GetDocuments([FromQuery] GetDocuments.Request request)
        {
            return await _mediator.Send(request);
        }
    }
}
