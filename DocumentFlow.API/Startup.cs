using DocumentFlow.Utils.Extensions;
using DocumentFlow.DataAccess.Interfaces.DataAccess;
using DocumentFlow.DataAccess.Implementation.Postgre;
using DocumentFlow.DataAccess.Implementation.Postgre.Repositories;
using DocumentFlow.UserCases;
using DocumentFlow.UserCases.Handlers.Documents.Mappings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocumentFlow.UseCases;
using DocumentFlow.UseCases.Managers;
using DocumentFlow.UseCases.Managers.Interfaces;
using DocumentFlow.UseCases.SettingsModels;
using DocumentFlow.UseCases.SettingsModels;
using Microsoft.Extensions.Logging;
using DocumentFlow.MiddlewarLoggingHttp;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Serilog;

namespace DocumentFlow.API
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddOptions();
            services.AddSwaggerGen(options =>
            {
                 options.CustomSchemaIds(type => type.ToString());
            });
            services.AddAutoMapper(typeof(DocumentTypeAutoMapperProfile));
            services.RegisterModule<DataAccessModule>(_configuration);
            services.RegisterModule<UseCasesModule>(_configuration);
            services.AddScoped<IDocumentManager, DocumentManager>();
            services.AddScoped<IFilesManager, FilesManager>();
            services.Configure<UploadingFilesSettings>(_configuration.GetSection("UploadingFiles"));
            services.Configure<KestrelServerOptions>(options =>
            {
                options.Limits.MaxRequestBodySize = int.MaxValue; // if don't set default value is: 30 MB
            });
            services.Configure<FormOptions>(x =>
            {
                x.ValueLengthLimit = int.MaxValue;
                x.MultipartBodyLengthLimit = int.MaxValue; // In case of multipart
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseAuthorization();
            app.UseMiddlewareLoggingHttpModule();
            app.UseSerilogRequestLogging();
            app.UseSwagger();
            app.UseSwaggerUI
               (x =>
                     {
                         x.SwaggerEndpoint("./v1/swagger.json", "DocumentFlow v1");
                         x.RoutePrefix = "swagger";
                     }
               );
            app.UseEndpoints(endpoints =>
         {
             endpoints.MapControllers();
         });
        }
    }
}
