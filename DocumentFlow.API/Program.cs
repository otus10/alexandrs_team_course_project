using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Formatting.Json;
using Serilog.Sinks.RabbitMQ;

using System;


namespace DocumentFlow.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IConfiguration configuration = new ConfigurationBuilder()
                                    .AddJsonFile("appsettings.json")
                                    .Build();
            Log.Logger = new LoggerConfiguration()
                            .ReadFrom.Configuration(configuration)
                            .WriteTo.RabbitMQ((clientConfiguration, sinkConfiguration) =>
                            {
                                {
                                    clientConfiguration.Port = 5672;
                                    clientConfiguration.DeliveryMode = RabbitMQDeliveryMode.Durable;
                                    clientConfiguration.Exchange = "Logs";
                                    clientConfiguration.Username = "guest";
                                    clientConfiguration.Password = "guest";
                                    clientConfiguration.ExchangeType = "direct";
                                    clientConfiguration.RouteKey = "Logs";
                                    clientConfiguration.Hostnames.Add("127.0.0.1");
                                    sinkConfiguration.TextFormatter = new JsonFormatter();
                                }
                            }).CreateLogger();
            try
            {
                Log.Information("Application Starting up");
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "The Application failed to start correctly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
